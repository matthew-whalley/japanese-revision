const db = require('../src/models');

function createUser(username, password, level, isAdmin = false) {
  return db.User.create({
    username: username,
    password: password,
    level: level,
    isAdmin,
    latestKanjiId: 1,
  });
}

function createLevel(number, name, description) {
  return db.Level.create({
    name,
    number,
    description,
  });
}

function createLesson(name, templateName,
    description, isDraft, jlpt, version, order, level) {
  return db.Lesson.create({
    name,
    templateName,
    description,
    isDraft,
    jlpt,
    version,
    order,
    LevelId: level,
  });
}

function createKanji({
  letter = '',
  onyomi = '',
  kunyomi = '',
  meaning = '',
  jlpt = 5,
  level = 1,
}) {
  return db.Kanji.create({
    letter, onyomi, kunyomi, meaning, jlpt, level,
  });
}

db.sequelize.sync({
  force: true,
  alter: true,
}).then(() => {
  console.log('DATABASE STRUCTURE BUILD COMPLETE');
  createUser('test', 'test', 1, false).then(() => {
    console.log('User created');
  });
  createUser('admin', 'test', 1, true).then(() => {
    console.log('Admin User created');
  });

  createLevel(1, 'Introduction to Japanese', 'intro to japanese').then(() => {
    console.log('level 1 created!!');
  });

  createLevel(2, 'Starting Grammar and making sentences',
      'starting to make sentences and get a grasp of the grammar structure',
  ).then(() => {
    console.log('level 2 created!!');
  });

  createLesson('Test', 'test', 'WIP content', false, 5, 1, 1, 1).then(() =>{
    console.log('Lesson created for Level 1');
  });

  createLesson('Test 2 Draft', 'test', 'WIP content',
      true, 5, 1, 1, 1).then(() =>{
    console.log('Draft Lesson created for Level 1');
  });

  createKanji({
    letter: '今',
    onyomi: 'いま',
    kunyomi: '',
    meaning: 'now',
    jlpt: 5,
    level: 1,
  });
});
