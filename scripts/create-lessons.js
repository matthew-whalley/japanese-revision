const db = require('../src/models');
const fs = require('fs');

function createLevel(number, name, description) {
  return db.Level.create({
    name,
    number,
    description,
  });
}

function createLesson(name, templateName,
    description, isDraft, jlpt, version, order, level) {
  return db.Lesson.create({
    name,
    templateName,
    description,
    isDraft,
    jlpt,
    version,
    order,
    LevelId: level,
  });
}

function removeExtension(filename) {
  return filename.substring(0, filename.lastIndexOf('.')) || filename;
}

function getTemplateNames(lessonTemplateFolder = './views/lessons') {
  return new Promise((resolve, reject)=>{
    fs.readdir(lessonTemplateFolder, {withFileTypes: false}, (err, files) => {
      if (err) reject(err);
      resolve(files);
    });
  });
};

async function setupLevels() {
  console.log('Level and Lesson table reset complete');

  const newLevel = await createLevel(100, 'Automated Levels',
      'test japanese level for admin testing',
  );
  console.log('level 1 created!!');

  const templates = await getTemplateNames();

  const lessonPromises = [];

  const levelId = newLevel.id;

  for (let i = 0; i < templates.length; i++) {
    const templateName = removeExtension(templates[i]);
    lessonPromises.push(
        createLesson(
            templateName, templateName, 'Auto generated',
            true, 5, 0, i, levelId
        ),
    );
  }
  await Promise.all(lessonPromises);
}

setupLevels();

