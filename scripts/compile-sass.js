const sass = require("sass");
const fs = require('fs');
const path = require("path");

function compileSass(inputFile = "./public/stylesheets/style.scss", outputFile = './dist/style.css', generateSourceMap = true, isMinified = true) {
  sass.render({
    file: inputFile,
    outFile: outputFile, // used for generating the source map
    sourceMap: generateSourceMap,
    outputStyle: isMinified ? "compressed" : "nested"
  }, function (err, result) {
    if (err) {
      console.error(err);
      return 1;
    }
    fs.writeFile(outputFile, result.css, function (error) {
      if (!error) {
        //file written on disk
        console.log("css file written successfully");
      } else {
        console.error(error);
        return 1;
      }
    });

    if (generateSourceMap) {
      fs.writeFile(outputFile + ".map", result.map, function (error) {
        if (!error) {
          //file written on disk
          console.log("map file written successfully");
        } else {
          console.error(error);
          return 1;
        }
      });
    }

  });
}


/**
 * Look ma, it's cp -R.
 * @param {string} src  The path to the thing to copy.
 * @param {string} dest The path to the new copy.
 */
function copyRecursiveSync(src, dest) {
  var exists = fs.existsSync(src);
  var stats = exists && fs.statSync(src);
  var isDirectory = exists && stats.isDirectory();
  if (isDirectory) {
    if (!exists) {
      fs.mkdirSync(dest);
    }
    fs.readdirSync(src).forEach(function (childItemName) {
      copyRecursiveSync(path.join(src, childItemName),
        path.join(dest, childItemName));
    });
  } else {
    fs.copyFileSync(src, dest);
  }
};

function copyJavascript() {
  copyRecursiveSync('public/javascript', 'dist');
  console.log("copied javascript successfully");
}

compileSass();
copyJavascript();
