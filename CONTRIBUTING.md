# How to Contribute

Thank you for looking at this page it means that you are looking to help and make the lives of japanese language learners easier. Explained below are the key standards and protocols that you'll need to know in order to get a merge request approved for the project. These standards are important as they allow for easier and standardized maintenance and development of the project.

## Getting Started

In order to get the project running you will need to have a mysql database. You can run this with other types of sql databases but mysql is the one that is used and supported. The specific instructions of how to get the project running locally are in the [README.md](/README.md) file.

## Coding Conventions

There is an eslint configuration file in the root of the project. please ensure that you have an IDE plugin to use this. linting is checked as part of the pipeline when submitting changes so this will fail if the formatting is incorrect.

## Testing

When approving merge requests depending on the size these may be tested manually by one of the core developers. The pipeline needs to pass in order for the changes to me merged in. Any new code should have code coverage of at least 80%. The script for getting the code coverage is setup in the package.json file. Run ``npm run test:coverage`` and this will generate the report in the /coverage folder, you can view this by opening the /coverage/index.html webpage. This gives a breakdown of exactly which lines are covered.

## Submitting Changes

When submitting merge requests please state specifically which gitlab issue this relates to or specifically what change or improvement you have done. If there isn't an issue already then we will create one. Please make sure that the merge request has commit squashing enabled and the delete branch set to true, and make sure that the merge request name follows the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/#summary) format with the issue number as the scope. e.g.
```
feat(#1): example merge request
```

Keeping commit messages inline with these changes and keeping merge requests with issues tagged in allows for the automatic creating of the CHANGELOG file when doing releases.

This project uses the gitflow workflow, so the develop branch should always be stable and shippable, and then release branches are created off the tip of develop. After the release has taken place those changes are then merged into the main branch and back into the develop branch.
