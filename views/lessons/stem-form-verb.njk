
        {# <nav class="nav nav-pills flex-column">
          <a class="nav-link" href="#stem">Verb stem</a>
          <a class="nav-link" href="#ru-verb">Ru verbs</a>
          <a class="nav-link" href="#u-verb">U verbs</a>
          <a class="nav-link" href="#suru-verb">Suru verbs</a>
        </nav> #}

<h2 id="stem">Verb stems</h2>

<p>So there are two basic forms which you'll see used first depending on the level of politeness. When learning verbs
you can learn either of these two forms first and then make the other forms from them. You also need to know what group the verb is in. </p>

<p>Verbs are either in the Godan group or the ichidan group. There are a few irregular verbs. The group that the verb is in
affects how the verb conjugates into other forms which are used to make other sentence structures.</p>

<p>There are a seperate group of verbs called suru verbs. These behave differently so we'll come onto these later</p>

<h3 id="ru-verb">Ichidan verbs (ru verbs)</h3>

<p>These verbs end in the "Ru" sound. These are the easiest to change into different forms. </p>

<p>The stem of the verb is the dictionary form of the verb with the Ru removed. an example of a ru verb is taberu(to eat)</p>

<p>If you want to say that "I eat" informally you just say "taberu". If you want to say that I eat in a more polite way you say
"tabemasu". You use the following pattern
(verb stem) + masu.</p>

<p>Below are some examples of ru verbs. the dictionary form and then the masu form (stem + masu)</p>

<ul>
  <li>taberu - tabemasu - to eat</li>
  <li>neru - nemasu - to sleep</li>
  <li>okiru - okimasu - to wake up</li>
  <li>miru - mimasu - to see</li>
  <li>kangaeru - kangaemasu - to think</li>
  <li>oshieru - oshiemasu - to teach</li>
  <li>iru - imasu - to exist (animate person/animal) </li>
  <li>deru - demasu - to leave/to come out</li>
  <li></li>
</ul>

<p>So remember that using the masu form is the polite way of talking. This is the initial form that most people use as beginners.
the dictionary form is normally used in more casual conversation. </p>

<h3 id="u-verb">Godan verbs (U verbs)</h3>

<p>These are the slightly more complicated types of verbs to conjugate. Go in this case a good way of remembering which one is which
the go in godan means 5 because there are 5 different levels in the hiragana table. ka,ki,ku,ke,ko and when we conjugate these we'll be
shifting the sounds in this table.</p>

<p>Dictionary forms of verbs will always have the u ending (the u form of the hiragana character), such as ku,gu,tsu,mu,bu etc. To put this in the
stem form just change it to the i version. e.g. ki,gi,chi,mi,bi. to put this in the masu form just add masu to the stem. </p>

<p>Below are some examples of changing some godan verbs into the masu form</p>

<ul>
  <li>nomu - nomimasu - to drink</li>
  <li>kaeru - kaerimasu - to return home</li>
  <li>oyogu - oyogimasu - to swim</li>
  <li>asobu - asobimasu - to play</li>
  <li>matsu - machimasu - to wait</li>
  <li>kau - kaimasu - to buy</li>
  <li>aru - arimasu - to exist(non-animate)</li>
  <li>hanasu - hanashimasu - to speak</li>
  <li>shinu - shinimasu - to die</li>
</ul>

<p>Notice that some verbs in the godan list end in ru. Not all verbs that end in ru are ichidan verbs, which is why you need to learn what group the verb is in if it
ends in ru, because it could be in either group depending on the verb.</p>


<h3 id="suru-verb">Suru verbs</h3>

<p>This is a special type of verbs. This is a bit like the equivalent in english of something that you do. suru is a verb that means to do. so
if you want to say that I play tennis you would say in Japanese that "I do tennis". So you would say "tenisu wo shimasu" or "tenisu wo suru"</p>

<p>Some examples of suru verbs below</p>

<ul>
  <li>sanpo suru - sanpo shimasu - to walk(to take a walk)</li>
  <li>kekkon suru - kekkon shimasu - to marry(to get married)</li>
  <li>benkyoo suru - benkyoo shimasu - to study</li>
  <li>chuumon suru - shuumon shimasu - to order/request (to make a order/request)</li>
  <li>tanoshimini suru tanoshimini shimasu - to look forward to something</li>
  <li>isshoni suru - isshoni shimasu - to unite/to join </li>
  <li>shinpai suru - shinpai shimasu - to worry </li>
  <li>yowaku suru - yowaku shimasu - to turn down (heat, sound gas etc)</li>
  <li>koi wo suru - koi wo shimasu - to fall in love</li>
  <li>mimi ni suru - mimi ni shimasu - to hear (to hear by chance/accident)</li>
  <li>hanashi wo suru - hanashi wo shimasu - to have a talk/conversation </li>
</ul>


<h2>Using the stem form</h2>

<p>So the first form we learnt is how to say the non-past polite version of a verb. I say non-past because this form can refer to the present and future
depending on the rest of the sentence.Some example sentences below of the masu form to show that it can be present and future</p>

<ul>
  <li>kyuu ji ni okimasu - I wake up at 9</li>
  <li>mainichi asagohan wo tabemasu - I eat breakfast everyday</li>
  <li>raigetsu ha nihon ni ikimasy - I will go to Japan next month</li>
  <li>ashita ha paatii wo shimasu - I will have a party tomorrow</li>
</ul>

<p>Now that you have learnt to convert the verbs to the masu form it is quite easy to change this into past tense, and negative form as well</p>

<table>
  <tr>
    <th>non-past positive</th>
    <th>non-past negative</th>
    <th>past positive</th>
    <th>past negative</th>
    <th>Verb meaning</th>
  </tr>
  <tr>
    <td>masu</td>
    <td>masen</td>
    <td>mashita</td>
    <td>masen deshita</td>
    <td></td>
  </tr>
  <tr>
    <td>tabemasu</td>
    <td>tabemasen</td>
    <td>tabemashita</td>
    <td>tabemasen deshita</td>
    <td>To eat</td>
  </tr>
  <tr>
    <td>nomimasu</td>
    <td>nomimasen</td>
    <td>nomimashita</td>
    <td>nomimasen deshita</td>
    <td>To drink</td>
  </tr>
  <tr>
    <td>benkyoo shimasu</td>
    <td>benkyoo shimasen</td>
    <td>benkyoo shimashita</td>
    <td>benkyoo shimasen deshita</td>
    <td>to study</td>
  </tr>
</table>
