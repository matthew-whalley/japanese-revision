# Japanese Revision

A site containing lessons and content for learning japanese, also a spaced repetition tool for learning japanese kanjia and vocabulary.


## Getting Started

By default this uses mysql2 database. so make sure that mysql is running and create a database. Then put the config defaults for the database in ``config/config.json``
the environment in the config is determined using the NODE_ENV environment variable. There are several ways of doing this. either you can use dotenv and set it in the .env file or you can just prepend ``NODE_ENV=development`` before the start command in a future step.

```
"development": {
    "username": "test",
    "password": "test",
    "database": "japanese",
    "host": "127.0.0.1",
    "dialect": "mysql"
  },
```
if you set the node env to test then the default database name in the config is japanese-test.

You will also need to call sequelize.sync() to generate the database tables and make sure the DB user has full permissions if this is the first time running it. In order to run this and create some seed data run

```
npm run build:db
```
you can set which database you want to build by changing theconfig and then running the same command above with the suffix or ``:test`` or ```:prod``` attached. These versions of the script just set the NODE_ENV environment variable, it is assumed that it is set to 'development' by default.


After checking out the project and setting up the database run the following commands to start the site locally. You should only need to run npm install once unless any of the dependencies
change between versions.

```
    npm install
    npm start
```

## Testing

### Unit testing

Unit tests go in test/unit folder. to run the unit tests run the command ``npm test`` this will run the tests using the framework mocha. If using an IDE like vscode then extensions are available to be able to run this from the editor. The config for it is the ``.mocharc.js`` file.

#### Code Coverage

After writing the unit tests you can use the command ```npm run test:coverage``` to check the code that has been ran. this uses a library called nyc to run the coverage tests. It's worth noting that this coverage only covers files that are touched by the tests, and not the entire codebase.

### E2E testing

The E2E tests are written in a framework called Cypress. In order to run the full E2E tests start an instance of the site
using one of the start commands. Before running the e2e tests you may want to setup a fresh database. To do this edit the test config in the ``config/config.json`` file with the database connection details. Make sure the user has the correct permissions to create and edit schemas. To generate the database run the command
```npm run script build:db:test```

You can also emit the test part, or use prod to use the other configurations.

To run the tests in one window start the site making sure that it is pointing to the correct database. to run while pointing to the test database run
``npm run start:test``.
In another command window while the site is running run the command
```npm run test:e2e:run```
this will run the tests and output the results into the console.

You may also want to use a test database or configure another local database when making schema changes and developing new features locally before merging into the develop branch. Any changes to the schema that require it should have a migration script written in the migrations folder. Examples of how to do this can be done are in the sequelize documentation.
