const express = require('express');
const setupTemplateEngine = require('./src/utils/templateHandler');
const setupConHandler = require('./src/utils/cronHandler.js').setup;
const router = require('./src/routes');
const { setupReqLogger, getLogger, LogTags } = require('./src/utils/logger.js');

const logger = getLogger(); 

const app = express();

app.disable('x-powered-by');

setupReqLogger(app);
setupTemplateEngine(app);
setupConHandler();

logger.info(LogTags.STARTUP, 'Setup Logger');

app.use('/assets', express.static('dist'));
app.use('/', router);

logger.info(LogTags.STARTUP, 'Setup Router');

module.exports = app;
