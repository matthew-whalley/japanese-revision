const {
  defineConfig,
} = require('cypress');

module.exports = defineConfig({

  e2e: {
    projectId: 'Japanese',
    baseUrl: 'http://localhost:3000',
    screenshotsFolder: 'test/e2e/screenshots',
    downloadsFolder: 'test/e2e/downloads',
    videosFolder: 'test/e2e/videos',
    specPattern: 'test/e2e/**/*.cy.{js,jsx,ts,tsx}',
    supportFile: 'test/e2e/support/e2e.{js,jsx,ts,tsx}',
    trashAssetsBeforeRuns: true,
    video: false,
    env: {},
  },
});
