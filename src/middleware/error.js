module.exports = (err, req, res, next) =>{
  if (res.headersSent) {
    return next(err);
  }
  res.status(500);
  req.log.error(err);
  res.render('pages/error.njk', {error: err});
};
