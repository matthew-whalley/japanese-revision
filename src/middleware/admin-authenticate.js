/**
 * redirects to the login page if the user doesn't exist. assumes
 *  *  the authenticationHAndler has been called on the router first
 * @param {*} req express request, with the user attached
 * @param {*} res express response. redirects if there is no user
 * @param {*} next thc callback to pass down to the page handler if
 *  the user exists
 */
module.exports = (req, res, next) => {
  if (req.isAuthenticated() && req.user.isAdmin === true) {
    next();
  } else {
    req.log.debug('redirecting user');
    res.redirect('/login');
  }
}; 
