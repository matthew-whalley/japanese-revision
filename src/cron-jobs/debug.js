const frequency = '*/10 * * * *';


function handler() {
  console.log('running a task every 10 minutes');
}

module.exports = {
  frequency,
  handler,
};
