const {
  getAllKanjiProgress,
} = require('../services/progress');
const {
  needsReview,
} = require('../utils/misc');
const frequency = '*/60 * * * * *';

/**
 * loop through all kanji that aren't ready for review and then
 * mark them as ready if they are.
 */
async function handler() {
  console.log('running progress update job');
  let updatedKanji = 0;


  try {
    const kanjiProgress = await getAllKanjiProgress();
    console.log('kanji fetched: ' + kanjiProgress.length);

    for (let i = 0; i < kanjiProgress.length; i++) {
      const progress = kanjiProgress[i];
      if (needsReview(new Date(progress.lastReview), progress.reviewNumber)) {
        progress.forReview = true;
        progress.save();
        updatedKanji += 1;
      }
    }
  } catch (err) {
    console.error(err);
  }

  console.log(`Kanji updated: ${updatedKanji}`);
}

module.exports = {
  frequency,
  handler,
};
