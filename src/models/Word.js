module.exports = function(sequelize, DataTypes) {
  const Word = sequelize.define('Word', {
    hiragana: DataTypes.STRING,
    katakana: DataTypes.STRING,
    meaning: DataTypes.STRING,
    romaji: DataTypes.STRING,
    kanji: DataTypes.STRING,
    description: DataTypes.STRING,
    jlpt: DataTypes.INTEGER,
    type: DataTypes.INTEGER,
  }, {
    timestamps: false,
  });

  return Word;
};
