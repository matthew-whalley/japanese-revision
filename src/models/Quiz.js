module.exports = function(sequelize, DataTypes) {
  const Quiz = sequelize.define('Quiz', {
    route: DataTypes.STRING,
    topics: DataTypes.STRING,
    isDraft: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    },
    length: {
      type: DataTypes.INTEGER,
      defaultValue: 10,
    },
    randomiseOrder: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  }, {
    timestamps: false,
    tableName: 'Quizzes',
  });

  return Quiz;
};
