module.exports = function(sequelize, DataTypes) {
  const Feedback = sequelize.define('Feedback', {
    email: DataTypes.STRING,
    feedback: DataTypes.TEXT,
    consent: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  }, {
    timestamps: true,
  });

  return Feedback;
};
