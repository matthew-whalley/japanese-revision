module.exports = function(sequelize, DataTypes) {
  const Lesson = sequelize.define('Lesson', {
    name: DataTypes.STRING,
    templateName: DataTypes.STRING,
    description: DataTypes.STRING,
    isDraft: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    jlpt: {
      type: DataTypes.INTEGER,
      defaultValue: 5,
    },
    version: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    order: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
  }, {
    timestamps: false,
  });

  return Lesson;
};
