module.exports = function(sequelize, DataTypes) {
  const Activity = sequelize.define('Activity', {
    description: DataTypes.STRING,
    triggerNotification: DataTypes.BOOLEAN,
  }, {
    timestamps: true,
  });

  return Activity;
};
