module.exports = function(sequelize, DataTypes) {
  const Level = sequelize.define('Level', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    number: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    jlpt: {
      type: DataTypes.INTEGER,
      defaultValue: 5,
    },
  }, {
    timestamps: false,
  });

  return Level;
};
