module.exports = function(sequelize, DataTypes) {
  const LevelProgress = sequelize.define('LevelProgress', {
    wordsCompleted: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    lastReview: {
      type: DataTypes.DATE,
      defaultValue: sequelize.NOW,
    },
  }, {
    timestamps: false,
  });

  return LevelProgress;
};
