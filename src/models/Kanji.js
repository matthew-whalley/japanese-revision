module.exports = function(sequelize, DataTypes) {
  const Kanji = sequelize.define('Kanji', {
    letter: DataTypes.STRING,
    onyomi: DataTypes.STRING,
    kunyomi: DataTypes.STRING,
    meaning: DataTypes.STRING,
    mnemonic: DataTypes.STRING,
    jlpt: DataTypes.INTEGER,
    level: DataTypes.INTEGER,
  }, {
    timestamps: false,
  });

  return Kanji;
};
