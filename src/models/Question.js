module.exports = function(sequelize, DataTypes) {
  const Quiz = sequelize.define('Question', {
    name: DataTypes.STRING,
    config: DataTypes.TEXT,
  }, {
    timestamps: false,
  });

  return Quiz;
};
