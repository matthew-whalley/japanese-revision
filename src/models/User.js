

function hashPassword(password) {
  console.log('calling hash password');
  return password;
}

module.exports = function(sequelize, DataTypes) {
  const User = sequelize.define('User', {
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    lastSignedOn: {
      type: DataTypes.DATE,
      defaultValue: sequelize.NOW,
    },
    level: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
    },
    latestKanjiId: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    isAdmin: {
      type: DataTypes.BOOLEAN,
      defaultValue: 0,
    },
  }, {
    indexes: [
      {
        name: 'users_username',
        unique: true,
        fields: ['username'],
      },
    ],
  });

  User.beforeCreate(async (user, options) => {
    const hashedPassword = await hashPassword(user.password);
    user.password = hashedPassword;
  });


  return User;
};
