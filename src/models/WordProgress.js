
module.exports = function(sequelize, DataTypes) {
  const WordProgress = sequelize.define('WordProgress', {
    lastReview: {
      type: DataTypes.DATE,
      defaultValue: sequelize.NOW,
    },
    reviewNumber: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    forReview: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    },
    correctReviews: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    incorrectReviews: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
  }, {
    timestamps: false,
  });

  return WordProgress;
};
