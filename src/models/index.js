'use strict';

const fs = require('fs');
const path = require('path');
const config = require('config');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const dbSetup = config.get('database');
const db = {};

const sequelize = new Sequelize(dbSetup);

sequelize.beforeConnect(()=>{
  console.log('connecting to DB');
});

sequelize.afterConnect(()=>{
  console.log('connected to DB');
});

fs
    .readdirSync(__dirname)
    .filter((file) => {
      return (file.indexOf('.') !== 0) &&
      (file !== basename) &&
      (file.slice(-3) === '.js');
    })
    .forEach((file) => {
      const model =
      require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
      db[model.name] = model;
    });

// define relationships here

db.User.hasMany(db.LevelProgress);
db.LevelProgress.belongsTo(db.User);

db.User.KanjiProgress = db.User.hasMany(db.KanjiProgress);
db.KanjiProgress.User = db.KanjiProgress.belongsTo(db.User);

db.User.hasMany(db.WordProgress);
db.WordProgress.belongsTo(db.User);

db.Word.hasMany(db.WordProgress);
db.WordProgress.belongsTo(db.Word);

db.Kanji.KanjiProgress = db.Kanji.hasMany(db.KanjiProgress);
db.KanjiProgress.Kanji = db.KanjiProgress.belongsTo(db.Kanji);

db.Level.words = db.Level.hasMany(db.Word);
db.Word.level = db.Word.belongsTo(db.Level);

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.User.hasMany(db.Activity);
db.Activity.belongsTo(db.User);

db.Level.hasMany(db.Lesson);
db.Lesson.belongsTo(db.Level);

db.Quiz.hasMany(db.Question);
db.Question.belongsTo(db.Quiz);

db.Lesson.hasMany(db.Quiz);
db.Quiz.belongsTo(db.Lesson);

module.exports = db;
