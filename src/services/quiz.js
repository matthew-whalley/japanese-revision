const db = require('../models');
const KrudService = require('./krudService');

const defaultEditValues = {
  route: '',
  topics: '',
  isDraft: false,
  length: 0,
  randomiseOrder: false,
};

/**
 * adds a question to the quiz
 * @param {number} quizId the id of the quiz to add the question to
 * @param {*} question the question to add to the quiz
 */
async function addQuestion(quizId, question) {
  const quiz = await db.Quiz.findByPk(quizId);
  await quiz.addQuestion(question);
}

async function getQuestions(quizId) {
  const quiz = await db.Quiz.findByPk(quizId);

  if(quiz === null){
    return null;
  }

  return await quiz.getQuestions();
}

async function getQuestionCount(quizId) {
  const quiz = await db.Quiz.findByPk(quizId);
  return await quiz.countQuestions();
}

const Quiz = {
  krud: new KrudService(db.Quiz, defaultEditValues),
  addQuestion,
  getQuestions,
  getQuestionCount,
};

module.exports = Quiz;
