const {
  Level,
} = require('../models');

async function getLevelByNumber(levelNumber) {
  const level = await Level.findOne({
    where: {
      number: levelNumber,
    },
  });
  return level;
};

async function getLevelById(id) {
  const level = await Level.findByPk(id);
  return level;
};

async function getAllLevels(pageSize = 20, pageNumber = 0) {
  const level = await Level.findAll({
    offset: pageNumber * pageSize,
    limit: pageSize,
    attributes: ['id', 'name', 'number', 'description', 'jlpt'],
  });
  return level;
};

async function getLevelCount() {
  const count = await Level.count();
  return count;
}

async function createLevel( {name, number, description, jlpt} = {}) {
  const level = await Level.create({
    name,
    number,
    description,
    jlpt,
  });

  return level;
}

function getEmptyLevel() {
  return Level.build({
    name: '',
    number: 0,
    description: '',
    jlpt: 5,
  });
}

async function updateLevel(level, id) {
  if (level.id == null) {
    console.log('can\'t update a level without an id');
  }

  const updatedLevel = await Level.update(level, {
    where: {
      id,
    },
  });

  return updatedLevel;
}

module.exports = {
  getAllLevels,
  getLevelById,
  getLevelByNumber,
  getLevelCount,
  getEmptyLevel,
  updateLevel,
  createLevel,
};
