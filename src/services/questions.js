const db = require('../models');
const KrudService = require('./krudService');

const defaultEditValues = {
  name: '',
  config: '',
};

const Questions = {
  krud: new KrudService(db.Question, defaultEditValues),
};

module.exports = Questions;
