const activities = require('./activities');
const levels = require('./levels');
const lessons = require('./lessons');
const progress = require('./progress');
const users = require('./users');
const kanji = require('./kanji');
const vocabulary = require('./vocabulary');

module.exports = {
  activities,
  levels,
  lessons,
  progress,
  users,
  vocabulary,
  kanji,
};
