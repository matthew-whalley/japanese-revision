const {Op} = require('sequelize');
const db = require('../models');


const defaults = {
  hiragana: '',
  katakana: '',
  meaning: '',
  romaji: '',
  kanji: '',
  description: '',
  jlpt: 5,
  type: 0,
  LevelId: 1,
};

async function addWord({
  hiragana = '',
  katakana = '',
  meaning = '',
  romaji = '',
  kanji = '',
  description = '',
  jlpt = 5,
  type = 0,
  level = 1,
} = null) {
  const word = await db.Word.create({
    hiragana,
    katakana,
    meaning,
    romaji,
    kanji,
    description,
    jlpt,
    type,
    LevelId: level,
  });

  return word;
}

async function deleteWord(id) {
  await db.Word.destroy({
    where: {
      id,
    },
  });
}

async function updateWord(word, id) {
  if (word.id === null) {
    console.log('can\'t update a word without an id');
    return;
  }

  const updatedWord = await db.Word.update(word, {
    where: {
      id,
    },
  });

  return updatedWord;
}

async function getAllVocabulary(pageSize = 20, pageNumber = 0) {
  const words = await db.Word.findAll(
      {
        offset: pageNumber * pageSize,
        limit: pageSize,
      },
  );

  return words;
}

function getEmptyWord() {
  return db.Word.build(defaults);
}

async function getWordById(id) {
  const word = await db.Word.findByPk(id);

  return word;
}

async function getVocabularyByLevel(levelId) {
  let vocabularyList;
  try {
    vocabularyList = await db.Word.findAll({
      where: {
        LevelId: levelId,
      },
    });
  } catch (err) {
    console.log(err);
  }

  return vocabularyList;
}

async function getVocabularyByKanji(kanji) {
  let vocabularyList;
  try {
    vocabularyList = await db.Word.findAll({
      where: {
        kanji: {
          [Op.like]: '%'+kanji+'%',
        },
      },
    });
  } catch (err) {
    console.log(err);
  }
  return vocabularyList;
}

async function getWordCount() {
  const count = await db.Word.count();

  return count;
}

module.exports = {
  addWord,
  getVocabularyByLevel,
  getAllVocabulary,
  getWordById,
  getWordCount,
  getVocabularyByKanji,
  updateWord,
  getEmptyWord,
  deleteWord,
};
