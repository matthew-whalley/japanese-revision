const {
  Lesson,
} = require('../models');
const KrudService = require('./krudService');

const fs = require('fs');

const defaults = {
  name: '',
  templateName: '',
  description: '',
  isDraft: false,
  version: 1,
  LevelId: 1,
  order: 0,
  jlpt: 5,
};

const Lessons = {
  krud: new KrudService(Lesson, defaults),
  getTemplateNames: function(lessonTemplateFolder = './views/lessons') {
    return new Promise((resolve, reject)=>{
      fs.readdir(lessonTemplateFolder, {withFileTypes: false}, (err, files) => {
        if (err) reject(err);
        resolve(files);
      });
    });
  },
  getPreviousLesson: async function(lessonId) {
    const previousId = lessonId - 1;
    try {
      const lesson = await Lesson.findByPk(previousId);
      return lesson;
    } catch (err) {
      return null;
    }
  },
  getNextLesson: async function(lessonId) {
    const nextId = lessonId + 1;
    try {
      const lesson = await Lesson.findByPk(nextId);
      return lesson;
    } catch (err) {
      return null;
    }
  },
  getLessonByLink: async function(lessonSlug) {
    const lessons = await Lesson.findAll({
      where: {
        templateName: lessonSlug,
      },
    });
    return lessons[0];
  },
  getLessonsByLevel: async function(levelId) {
    const lessons = await Lesson.findAll({
      where: {
        LevelId: levelId,
      },
    });
    return lessons;
  },
};

module.exports = Lessons;
