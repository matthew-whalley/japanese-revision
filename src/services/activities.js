const db = require('../models');

async function postActivity(userId, description, notification = false) {
  const activity = await db.Activity.create({
    UserId: userId,
    description,
    triggerNotification: notification,
  });

  return activity;
}


module.exports = {
  postActivity,
};
