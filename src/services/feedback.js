const {
  Feedback,
} = require('../models');


async function getAllFeedback(pageSize = 20, pageNumber = 0) {
  const fb = await Feedback.findAll(
      {
        offset: pageNumber * pageSize,
        limit: pageSize,
      });

  return fb;
}

async function addFeedback(email, feedback, consent) {
  const fb = await Feedback.create({
    email: email,
    feedback: feedback,
    consent: consent,
  });

  return fb;
}

async function getFeedbackCount() {
  const count = await Feedback.count();

  return count;
}

async function getFeedbackById(id) {
  const feedback = await Feedback.findByPk(id);

  return feedback;
}


module.exports = {
  getAllFeedback,
  addFeedback,
  getFeedbackCount,
  getFeedbackById,
};

