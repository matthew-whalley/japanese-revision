const {
  Kanji,
} = require('../models');


/**
 *
 * @param {String} kanji the character to search for
 * @return {*} an instance of the kanji model with the letter
 */
async function getKanji(kanji) {
  const character = await Kanji.findOne({
    where: {
      letter: kanji,
    },
  });
  return character;
}

async function getKanjiById(id) {
  const character = await Kanji.findByPk(id);

  return character;
}

async function getAllKanji(pageSize = 20, pageNumber = 0) {
  const characters = await Kanji.findAll(
      {
        offset: pageNumber * pageSize,
        limit: pageSize,
      },
  );

  return characters;
}

async function getKanjiCount() {
  const count = await Kanji.count();

  return count;
}

async function createKanji(kanji) {
  kanji.id == null;
  const character = await Kanji.create(kanji);

  return character;
}

async function updateKanji(kanji, id) {
  if (kanji.id === null) {
    console.log('can\'t update a kanji without an id');
    return;
  }

  const character = await Kanji.update(kanji, {
    where: {
      id,
    },
  });

  return character;
}


module.exports = {
  getKanji,
  getKanjiById,
  getAllKanji,
  getKanjiCount,
  createKanji,
  updateKanji,
};
