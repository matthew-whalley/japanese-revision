const {
  User,
  KanjiProgress,
  sequelize,
} = require('../models');
const {
  getKanjiById,
} = require('./kanji');

/**
 * return the user by username with all the progress information
 * @param {String} username the username of the user to fetch
 * @return {*} User
 */
async function getUserByUsername(username) {
  const user = await User.findOne({
    where: {
      username: username,
    },
    attributes: {
      exclude: ['password', 'isAdmin'],
    },
  });
  return user;
}

async function createUser(
    username,
    password,
    level = 1,
    isAdmin = false,
    initialKanji = 15) {
  let user;

  const t = await sequelize.transaction();
  try {
    user = await User.create({
      username: username,
      password: password,
      level: level,
      latestKanjiId: initialKanji,
      isAdmin,
    }, {
      transaction: t,
    });

    const userId = user.id;

    const kanjiProgress = [];
    for (let kanjiId = 1; kanjiId < (initialKanji + 1); kanjiId++) {
      kanjiProgress.push({
        UserId: userId,
        KanjiId: kanjiId,
      });
    }

    await KanjiProgress.bulkCreate(kanjiProgress, {
      transaction: t,
    });

    await t.commit();
  } catch (error) {
    await t.rollback();
  }

  return user;
}

async function assignKanjiBatch(userId, kanjiNo = 5) {
  const user = await User.findByPk(userId, {});
  const lastKanjiId = user.latestKanjiId;

  const kanjiProgress = [];
  let kanjiInserted = 0;
  for (let i = 1; i < (kanjiNo + 1); i++) {
    if (await getKanjiById(lastKanjiId + i) == null) {
      console.log('No more kanji to add');
      break;
    }

    kanjiProgress.push({
      UserId: userId,
      KanjiId: lastKanjiId + i,
    });

    kanjiInserted = i;
  }

  const t = await sequelize.transaction();
  if (kanjiInserted > 0) {
    try {
      await KanjiProgress.bulkCreate(kanjiProgress, {
        transaction: t,
      });
      await user.update({
        latestKanjiId: lastKanjiId + kanjiInserted,
      }, {
        transaction: t,
      });

      await t.commit();
    } catch (error) {
      await t.rollback();
    }
  }
  return kanjiInserted;
}

/**
 * returns a user with just the properties needed for authentication such as
 * username and password
 * @param {String} username the username of the user to fetch
 * @return {*} a User object with limited parameters
 */
async function getUserAuthByUsername(username) {
  const user = await User.findOne({
    where: {
      username: username,
    },
    attributes: ['username', 'password', 'isAdmin', 'id'],
  });

  return user;
}

async function getAllUsers(pageSize = 20, pageNumber = 0) {
  const users = await User.findAll({
    offset: pageNumber * pageSize,
    limit: pageSize,
    attributes: ['username', 'isAdmin', 'level', 'lastSignedOn', 'id'],
  });

  return users;
}

async function getUserCount() {
  const count = await User.count();
  return count;
}

async function deleteUser(id) {
  const t = await sequelize.transaction();
  try {
    await User.destroy({
      where: {
        id,
      },
    });
    // remove the kanji progress from the users

    await t.commit();
  } catch (error) {
    await t.rollback();
  }
}

module.exports = {
  getUserByUsername,
  getUserAuthByUsername,
  createUser,
  deleteUser,
  assignKanjiBatch,
  getAllUsers,
  getUserCount,
};
