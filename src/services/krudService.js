class KrudService {
  constructor(model, defaults = {}) {
    this.model = model;
    this.defaults = defaults;
  }

  setDefaults(defaultValues) {
    this.defaults = defaultValues;
  }

  async getById(id) {
    const obj = await this.model.findByPk(id);
    return obj;
  }

  async create(object) {
    const obj = await this.model.create(object);
    return obj;
  }

  async getCount() {
    const count = await this.model.count();
    return count;
  }

  async getAll(pageNumber = 0, pageSize = 20, attributes) {
    const objects = await this.model.findAll(
        {
          offset: pageNumber * pageSize,
          limit: pageSize,
          attributes,
        },
    );
    return objects;
  }

  getEmpty() {
    return this.model.build(this.defaults);
  }

  async update(obj, id) {
    if (obj.id == null) {
      console.log('cannot update object without id');
    }

    const updatedObj = await this.model.update(obj, {
      where: {
        id,
      },
    });

    return updatedObj;
  }

  async remove(id) {
    await this.model.destroy({
      where: {
        id,
      },
    });
  }
}

module.exports = KrudService;
