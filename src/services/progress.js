const sequelize = require('sequelize');
const db = require('../models');


async function getAllKanjiProgress() {
  let progress;
  try {
    progress = await db.KanjiProgress.findAll({
      where: {
        forReview: false,
      },
    });
  } catch (error) {
    console.log(error);
  }
  return progress;
}

async function getKanjiProgress(userId) {
  let progress;
  try {
    progress = await db.KanjiProgress.findAll({
      where: {
        UserId: userId,
        forReview: true,
      },
      include: [{
        model: db.Kanji,
      }],
    });
  } catch (error) {
    console.log(error);
  }
  return progress;
}

async function getWordProgress(userId) {
  let progress;
  try {
    progress = await db.WordProgress.findAll({
      where: {
        UserId: userId,
        forReview: true,
      },
      include: [{
        model: db.Word,
      }],
    });
  } catch (error) {
    console.log(error);
  }
  return progress;
}

async function kanjiAnsweredCorrect(userId, kanjiId) {
  let progress;
  try {
    progress = await db.KanjiProgress.findOne({
      where: {
        UserId: userId,
        KanjiId: kanjiId,
      },
    });

    console.log('found kanji progress to update');

    if (progress.forReview) {
      progress.correctReviews++;
      progress.forReview = false;
      progress.reviewNumber++;
      progress.lastReview = Date.now();
      progress.save();
    }
  } catch (error) {
    console.log(error);
  }
}

async function kanjiAnsweredIncorrect(userId, kanjiId) {
  let progress;
  try {
    progress = await db.KanjiProgress.findOne({
      where: {
        UserId: userId,
        KanjiId: kanjiId,
      },
    });

    console.log('found kanji progress to update');

    if (progress.forReview) {
      progress.incorrectReviews++;
      progress.forReview = false;
      progress.lastReview = Date.now();


      if (progress.reviewNumber > 0) {
        progress.reviewNumber--;
      }
      progress.save();
    }
  } catch (error) {
    console.log(error);
  }
}


// SELECT reviewNumber, COUNT(*)
// FROM kanjiprogresses
// WHERE UserId = 2
// GROUP BY reviewNumber
// ORDER BY reviewNumber DESC;

async function getKanjiReviewStages(userId) {
  let progress;
  try {
    progress = await db.KanjiProgress.findAll({
      attributes: [
        [sequelize.fn('COUNT', sequelize.col('*')), 'kanji'],
        'reviewNumber',
      ],
      where: {
        UserId: userId,
      },
      group: 'reviewNumber',
      order: [
        ['reviewNumber', 'DESC'],
      ],
    });
  } catch (error) {
    console.log(error);
  }
  return progress;
}

module.exports = {
  getKanjiProgress,
  getAllKanjiProgress,
  getKanjiReviewStages,
  getWordProgress,
  kanjiAnsweredCorrect,
  kanjiAnsweredIncorrect,
};
