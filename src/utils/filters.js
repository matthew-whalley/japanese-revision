const {matchContainerInnerText} = require('./misc');


/**
 *
 * @param {String} text the string template to replace the property
 * @param {*} object the object that contains the property
 * @return {String} the string with the property replaced into it
 */
function replaceProperties(text, object) {
  const containerProperties = matchContainerInnerText(text);

  let newText = text;
  containerProperties.forEach((property) => {
    newText = newText.replace(/[<]{2}:([a-zA-Z1-9]*)[>]{2}/, object[property]);
  });

  return newText;
}

module.exports = {
  replaceProperties,
};
