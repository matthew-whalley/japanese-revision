const reviewSchedules = {
  0: '1h',
  1: '1d',
  2: '3d',
  3: '1w',
  4: '2w',
  5: '1m',
  6: '4m',
  7: '8m',
  8: '1y',
};

const reviewLevels = {
  'new': 2,
  'recognized': 4,
  'learnt': 6,
  'mastered': 10,
};

const getLevelDescriptionFromSchedule = (level) => {
  for (key of Object.keys(reviewLevels)) {
    if (level <= reviewLevels[key]) {
      return key;
    }
  };

  return 'mastered';
};

const validateEmail = (email) => {
  return email.match(
      // eslint-disable-next-line max-len
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  );
};

function needsReview(lastReviewed, reviewLevel) {
  const difference = compareDates(lastReviewed, Date.now());

  if (reviewLevel >= Object.keys(reviewSchedules).length) {
    return false;
  }

  const allowedDifference = convertTimeToMS(reviewSchedules[reviewLevel]);

  return difference >= allowedDifference;
}

function compareDates(date1, date2) {
  const difference = date2 - date1;
  return Math.abs(difference);
}

function convertTimeToMS(timeString) {
  const number = timeString.match(/[0-9]+/g);
  const measurement = timeString.match(/[a-zA-Z]+/g);

  const multipliers = {
    'ms': 1,
    's': 1000,
    'min': 1000 * 60,
    'h': 1000 * 60 * 60,
    'd': 1000 * 60 * 60 * 24,
    'w': 1000 * 60 * 60 * 7,
    'm': 1000 * 60 * 60 * 7 * 4,
    'y': 1000 * 60 * 60 * 24 * 365,
  };

  return number * multipliers[measurement];
}

function isCorrectFromList(answer, correctAnswers) {
  const answers = correctAnswers.split(',');

  let a = 0;
  for (a = 0; a < answers.length; a++) {
    if (answers[a].trim().toLowerCase() === answer.trim().toLowerCase()) {
      return true;
    }
  }

  return false;
}

/**
 *
 * @param {String} text the text containing the placeholder
 * @return {String[]} the text inside the brackets
 */
function matchWholeContainers(text) {
  const captureRegex = /[<]{2}:([a-zA-Z1-9]*)[>]{2}/g;
  const matches = text.match(captureRegex);

  return matches == null ? [] : matches;
}

function matchContainerInnerText(text) {
  const captureRegex = /(?<=<<:)[a-zA-Z1-9]*/g;
  const matches = text.match(captureRegex);

  return matches == null ? [] : matches;
}

function removeExtension(filename) {
  return filename.substring(0, filename.lastIndexOf('.')) || filename;
}

module.exports = {
  getLevelDescriptionFromSchedule,
  reviewLevels,
  needsReview,
  compareDates,
  convertTimeToMS,
  isCorrectFromList,
  validateEmail,
  removeExtension,
  matchWholeContainers,
  matchContainerInnerText,
};
