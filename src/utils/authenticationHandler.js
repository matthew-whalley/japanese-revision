const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const bodyParser = require('body-parser');

const {getUserAuthByUsername} = require('../services/users');

function setupPassport() {
  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  passport.deserializeUser(function(user, done) {
    done(null, user);
  });

  switch (process.env.NODE_ENV) {
    case 'test':
      passport.use(new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
      },
      async function(username, password, done) {
        try {
          let user;
          console.log('USING MOCK AUTHANTICATION!!');
          if (username === 'admin') {
            user = {
              id: 1,
              username: 'admin',
              password: 'test',
              isAdmin: true,
            };
          }

          if (username === 'test') {
            user = {
              id: 2,
              username: 'test',
              password: 'test',
              isAdmin: false,
            };
          }
          return done(null, user);
        } catch (err) {
          return done(err);
        }
      },
      ));
      break;
    default:
      passport.use(new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
      },
      async function(username, password, done) {
        try {
          const user = await getUserAuthByUsername(username);

          if (!user) {
            console.log('incorrect username');
            return done(null, false, {message: 'Incorrect username.'});
          }
          if (user.password !== password) {
            console.log('incorrect password');
            return done(null, false, {message: 'Incorrect password.'});
          } else {
            console.log('authenticated successfully');
            return done(null, user);
          }
        } catch (err) {
          console.log(err);
          return done(err);
        }
      },
      ));
  }
}

function routerAuthentication(router) {
  setupPassport();
  router.use(bodyParser.urlencoded({extended: false}));
  router.use(passport.initialize());
  router.use(passport.session());
}

function login() {
  return passport.authenticate(
      'local',
      {
        successRedirect: '/dashboard',
        failureRedirect: '/login',
        failureFlash: false,
      },
  );
}

async function logout(req, res, next) {
  await req.logOut();
  next();
}

module.exports = {
  setup: routerAuthentication,
  login: login,
  logout: logout,
};
