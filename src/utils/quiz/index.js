const Quiz = require("./quiz");
const { QuestionGenerator } = require("./question");

module.exports = {
  Quiz,
  QuestionGenerator
}
