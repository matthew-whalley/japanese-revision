const {isCorrectFromList} = require('../misc');

const QuestionTypes = {
  KANJI: 'kanji',
  GENERAL: 'general',
};

class QuestionGenerator {
  constructor() {
    this.questions = [];
    this.lastQuestion = 0;
  }


  getQuestions() {
    return this.questions;
  }

  addKanjiQuestion(kanji) {
    this.questions.push({
      type: QuestionTypes.KANJI,
      id: this.lastQuestion,
      kanji: kanji.id,
      question: `${kanji.letter} meaning`,
      answer: kanji.meaning,
      expectJapanese: false,
      isCorrect: isCorrectFromList,
    });
    this.lastQuestion++;
  }

  addGenericQuestion(question, answer, expectJapanese) {
    this.questions.push( {
      type: QuestionTypes.GENERAL,
      id: this.lastQuestion,
      question: question,
      answer: answer,
      expectJapanese: expectJapanese,
      isCorrect: isCorrectFromList,
    });
    this.lastQuestion++;
  }
}


module.exports = {
  QuestionGenerator,
  QuestionTypes,
};
