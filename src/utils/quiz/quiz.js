const express = require('express');
const authenticate = require('../../middleware/authenticate');

function onQuizStart(req, quiz) {}

async function onCorrect() {
  return true;
}

async function onIncorrect() {
  return true;
}

async function onQuizComplete() {
  return true;
}


async function onQuizPageLoad() {}

class Quiz {
  constructor({
    route = '/',
    config = {
      requireAuth: true,
    },
    questions = [],
    onCorrectAnswer = onCorrect,
    onIncorrectAnswer = onIncorrect,
    onQuizLoad = onQuizStart,
    onComplete = onQuizComplete,
    onPageLoad = onQuizPageLoad,
  }) {
    // eslint-disable-next-line new-cap
    this.router = express.Router();
    this.route = route;
    this.config = config;

    if (this.config.requireAuth == true) {
      this.router.use(authenticate);
    }

    this.questions = questions;

    this.router.get(this.route, async (req, res, next) => {
      try {
        await onQuizLoad(req, res, this);
        await onPageLoad(req, res);
        this.checkQuestions();
        const question = this.getFirstQuestion();
        const nextQuestion = this.getNextQuestion(1);
        question.disabled = false;
        res.render('pages/quiz.njk', {
          question,
          hasNextQuestion: nextQuestion !== undefined,
          postRoute: req.baseUrl,
          header: 'Kanji Quiz',
          user: req.user,
          errors: [],
        });
      } catch(err) {
        req.log.error(err);
        next();
      }
    });

    this.router.post(this.route, [
      express.json(),
      async (req, res, next) => {
        try {
          const question = this.getQuestion(req.body.id);
          const userId = req.user ? req.user.id : null;
          if (this.isCorrect(question, req.body.answer)) {
            const nextQuestion = this.getNextQuestion(req.body.id);
            if (await onCorrectAnswer(question, req, userId)) {
              // render next question here
              if (nextQuestion !== undefined) {
                if (nextQuestion) {
                  nextQuestion.disabled = false;
                }

                await onPageLoad(req, res);

                res.render('pages/quiz.njk', {
                  question: nextQuestion,
                  postRoute: req.baseUrl,
                  header: 'Kanji Quiz',
                  user: req.user,
                  errors: [],
                });
              }
            }
            if (nextQuestion == undefined) {
              if (await onComplete()) {
                // render quiz complete page
                await onPageLoad(req, res);

                res.render('pages/complete.njk', {
                  user: req.user,
                });
              }
            }
          } else {
            if (await onIncorrectAnswer(question, req, userId)) {
              const nextQuestion = this.getNextQuestion(req.body.id);

              const q = JSON.parse(JSON.stringify(question));
              q.disabled = true;
              q.previousAnswer = req.body.answer;

              const data = {
                question: q,
                hasNextQuestion: nextQuestion !== undefined,
                errors: {
                  ['answer-'+ q.id]: {
                    text: 'Incorrect Answer, The correct answer is: ' + q.answer,
                  },
                },
                postRoute: req.baseUrl,
                header: 'Kanji Quiz',
                user: req.user,
              };

              await onPageLoad(req, res);

              res.render('pages/quiz.njk', data);
            }
          }
        } catch(err) {
          req.log.error(err);
          next();
        }
      },
    ]);
  }


  checkQuestions() {
    if (this.questions.length == 0) {
      throw Error('Cannot create quiz without questions');
    }

    if (this.getFirstQuestion() == undefined) {
      throw Error('Please specify a question with id 0');
    }
  }

  getFirstQuestion() {
    return this.questions.filter((question) => {
      return question.id == 0;
    })[0];
  }

  getQuestion(questionId) {
    return this.questions.filter((question) => {
      return question.id == questionId;
    })[0];
  }

  getNextQuestion(previousQuestionId) {
    return this.questions.filter((question) => {
      return question.id == Number(previousQuestionId) + 1;
    })[0];
  }

  isCorrect(question, answer) {
    if (question.isCorrect !== undefined) {
      return question.isCorrect(answer, question.answer);
    }
    return answer === question.answer;
  }

  getRouter() {
    return this.router;
  }
}


module.exports = Quiz;
