const cron = require('node-cron');

const debugTask = require('../cron-jobs/debug');
const updateProgressTask = require('../cron-jobs/update-progress');

function startTask(frequency, handler, scheduled) {
  const task = cron.schedule(frequency, handler, {
    scheduled: scheduled,
  });

  task.start();

  return task;
}

function stopTask(task) {
  task.stop();
  return task;
}


function setup() {
  console.log('setting up cron jobs');
  startTask(debugTask.frequency, debugTask.handler, true);
  startTask(updateProgressTask.frequency, updateProgressTask.handler, true);
}


module.exports = {
  setup,
  startTask,
  stopTask,
};
