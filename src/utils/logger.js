const pino = require('pino');
const { randomUUID } = require('node:crypto')
const pinoHttp = require('pino-http');
const { version } = require('../../package.json');

const LogTags = {
    STARTUP: { tags: 'startup' },
    REQUEST: { tags: 'request'},
    CRON: { tags: 'cron'}
}

// point this to an environment variable options are 
// 'fatal', 'error', 'warn', 'info', 'debug', 'trace', 'silent'
const logSetupOptions = {
    level: 'info',
    mixin() {
        return {
            appName: "Learn Japanese",
            version,
            ...LogTags.REQUEST
        };
    }
}

function setupReqLogger(app) {
    const httpLogger = pinoHttp(
        {
            ...logSetupOptions,
            genReqId: function (req, res) {
                const existingID = req.id ?? req.headers["x-request-id"]
                if (existingID) return existingID
                const id = randomUUID()
                res.setHeader('X-Request-Id', id)
                return id
              }
        });
    app.use(httpLogger);
}

function getLogger(){
    return pino(logSetupOptions);
}

module.exports = {
    setupReqLogger,
    getLogger,
    LogTags
}