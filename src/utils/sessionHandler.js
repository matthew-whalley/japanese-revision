const session = require('express-session');
const FileStore = require('session-file-store')(session);

function setup(router) {
  const fileStoreOptions = {
    path: './sessions',
    retries: 3,
  };

  router.use(session({
    store: new FileStore(fileStoreOptions),
    secret: 'cats',
    resave: true,
    saveUninitialized: true,
  }),
  );
}

module.exports = {setup};
