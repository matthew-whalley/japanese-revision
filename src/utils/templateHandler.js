const nunjucks = require('nunjucks');
const path = require('path');
const routes = require('../routes/routes');
const {replaceProperties} = require('./filters');

function setupTemplateEngine(app) {
  const views = [
    path.join(__dirname, '../../views'),
  ];

  app.set('views', views);
  app.set('view engine', 'njk');

  const loader = new nunjucks.FileSystemLoader(
      app.get('views'),
      {
        watch: true,
        noCache: true,
        autoescape: true,
      },
  );

  const environment = new nunjucks.Environment(loader, {
    throwOnUndefined: false,
    trimBlocks: true,
    lstripBlocks: true,
    noCache: true,
  });

  environment.addGlobal('Routes', routes);
  environment.addGlobal('getContext', function(name) {
    console.log(name);
    return (name) ? this.ctx[name] : this.ctx;
  });

  environment.addFilter('replaceProperties', replaceProperties);

  environment.express(app);

  return environment;
}

module.exports = setupTemplateEngine;
