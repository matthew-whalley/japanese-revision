const express = require('express');

const routes = require('./routes');

// import page routers
const dashboardRouter = require('./dashboard');
const accountRouter = require('./account');
const levelRouter = require('./level');
const lessonRouter = require('./lesson');
const adminRouter = require('./admin');
const kanjiRouter = require('./kanji');
const vocabRouter = require('./vocabulary');
const feedbackRouter = require('./feedback');
const authenticationRouter = require('./authentication');

// import middleware
const errorHandler = require('../middleware/error');
const adminAuthenticate = require('../middleware/admin-authenticate');

// session and authentication setup scripts
const sessionHandler = require('../utils/sessionHandler');
const authenticationHandler = require('../utils/authenticationHandler');


// eslint-disable-next-line new-cap
const baseRouter = express.Router();
// eslint-disable-next-line new-cap
const pageRouter = express.Router();

sessionHandler.setup(pageRouter);
authenticationHandler.setup(pageRouter);

pageRouter.use(routes.ADMIN, [
  adminAuthenticate,
  adminRouter,
]);


pageRouter.use('/',
    [
      dashboardRouter,
      kanjiRouter,
      accountRouter,
      levelRouter,
      vocabRouter,
      lessonRouter,
      feedbackRouter,
    ]);

pageRouter.get('/', (req, res)=>{
  res.redirect('/login');
});

baseRouter.use('/', authenticationRouter);
baseRouter.use('/', pageRouter);
baseRouter.use(errorHandler);

module.exports = baseRouter;
