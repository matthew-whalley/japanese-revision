const Questions = require('../../services/questions');
const Quiz = require('../../services/quiz');


async function list(req, res) {
  let objects;
  const pageSize = 50;
  let pageNumber;
  const count = await Questions.krud.getCount();

  const objectName = 'Questions';
  const columns = [
    {name: 'Name', value: 'name'},
  ];
  const itemActions = [
    {
      text: 'Edit',
      link: '/admin/question/<<:id>>',
    },
    {
      text: 'Delete',
      link: '/admin/question/<<:id>>/delete',
    },
  ];

  const actions = [{
    text: 'Add Questions',
    link: '/admin/question/-1',
  }];

  if (req.query.page !== undefined && Number(req.query.page > 0)) {
    objects = await Questions.krud.getAll(Number(req.query.page) - 1, pageSize);
    pageNumber = Number(req.query.page)- 1;
  } else {
    objects = await Questions.krud.getAll(0, pageSize);
    pageNumber = Number(0);
  }


  res.render('pages/admin-list.njk', {
    user: req.user,
    objects,
    pageSize,
    pageNumber,
    count,
    objectName,
    actions,
    columns,
    itemActions,
  });
};

async function edit(req, res) {
  let object;
  const objectName = 'Question';
  const objectPostRoute = '/admin/question';

  if (Number(req.params.id) > -1) {
    object = await Questions.krud.getById(Number(req.params.id));
    config = JSON.parse(object.config);
    object.type = config.type;
    object.question = config.question;
    object.answer = config.answer;
  } else {
    object = Questions.krud.getEmpty();
    object.type = 'general';
    object.question = '';
    object.answer = '';
  }

  const quizes = await Quiz.krud.getAll(0, 200);

  const properties = [
    {label: 'name', field: 'name', type: 'text'},
    {
      label: 'Question config',
      field: 'config',
      type: 'toggleSelect',
      value: object.type ? object.type: 'general',
      options: [
        {
          name: 'General',
          value: 'general',
          properties: [
            {
              label: 'Question',
              field: 'question', type: 'text', value: 'question',
            },
            {
              label: 'Answer', field: 'answer', type: 'text', value: 'answer',
            },
          ],
        },
        {
          name: 'Kanji',
          value: 'kanji',
          properties: [],
        },
        {
          name: 'Vocabulary',
          value: 'vocabulary',
          properties: [],
        },
      ],
    },
    {
      label: 'Quiz',
      field: 'QuizId',
      type: 'multi',
      data: quizes,
      selectType: 'radio',
      label: 'route',
      fields: ['id', 'route'],
    },
  ];


  res.render('pages/admin-edit.njk', {
    user: req.user,
    object,
    properties,
    objectName,
    objectPostRoute,
  });
};

async function editPost(req, res) {
  try {
    req.body.config = JSON.stringify({
      type: req.body.type,
      question: req.body.question,
      answer: req.body.answer,
    });

    if (req.body.id !== null && req.body.id > -1) {
      await Questions.krud.update(req.body, req.body.id);
    } else {
      await Questions.krud.create(req.body);
    }

    res.redirect('/admin/question');
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
  }
};

async function remove(req, res) {
  console.log(req.params.id);

  await Questions.krud.remove(req.params.id);

  res.redirect('/admin/question');
};

module.exports = {
  list,
  edit,
  editPost,
  remove,
};
