const {getAllVocabulary, getWordCount,
  getWordById, getEmptyWord,
  updateWord, addWord, deleteWord} = require('../../services/vocabulary');

async function list(req, res) {
  let objects;
  const pageSize = 50;
  let pageNumber;
  const count = await getWordCount();

  const objectName = 'Words';
  const columns = [
    {name: 'Meaning', value: 'meaning'},
    {name: 'Hiragana', value: 'hiragana'},
    {name: 'Kanji', value: 'kanji'},
    {name: 'Romaji', value: 'romaji'},
    {name: 'JLPT', value: 'jlpt'},
    {name: 'Level', value: 'LevelId'},
  ];
  const itemActions = [
    {
      text: 'Edit',
      link: '/admin/word/<<:id>>',
    },
    {
      text: 'Delete',
      link: '/admin/word/<<:id>>/delete',
    },
  ];

  const actions = [{
    text: 'Add Word',
    link: '/admin/word/-1',
  }];

  if (req.query.page !== undefined && Number(req.query.page > 0)) {
    objects = await getAllVocabulary(pageSize, Number(req.query.page) - 1);
    pageNumber = Number(req.query.page)- 1;
  } else {
    objects = await getAllVocabulary(pageSize, 0);
    pageNumber = Number(0);
  }


  res.render('pages/admin-list.njk', {
    user: req.user,
    objects,
    pageSize,
    pageNumber,
    count,
    objectName,
    actions,
    columns,
    itemActions,
  });
};

async function edit(req, res) {
  let object;
  const objectName = 'Word';
  const objectPostRoute = '/admin/word';

  if (Number(req.params.id) > -1) {
    object = await getWordById(Number(req.params.id));
  } else {
    object = getEmptyWord();
  }

  const properties = [
    {label: 'Meaning', field: 'meaning', type: 'text'},
    {label: 'Hiragana', field: 'hiragana', type: 'text'},
    {label: 'Katakana', field: 'katakana', type: 'text'},
    {label: 'Kanji', field: 'kanji', type: 'text'},
    {label: 'Romaji', field: 'romaji', type: 'text'},
    {label: 'Description', field: 'description', type: 'text'},
    {label: 'JLPT', field: 'jlpt', type: 'number'},
    {label: 'Level', field: 'LevelId', type: 'number'},
  ];


  res.render('pages/admin-edit.njk', {
    user: req.user,
    object,
    properties,
    objectName,
    objectPostRoute,
  });
};

async function editPost(req, res) {
  try {
    // each word needs either a hiragana or a katakana
    // kanji is optional
    // description is optional
    // JLPT is optional, Level is required
    if (req.body.id !== null && req.body.id > -1) {
      console.log('updating word');
      await updateWord(req.body, req.body.id);
    } else {
      console.log('adding word');
      await addWord(req.body);
    }


    res.redirect('/admin/word');
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
  }
};

async function remove(req, res) {
  console.log(req.params.id);

  await deleteWord(req.params.id);

  res.redirect('/admin/word');
};

module.exports = {
  list,
  edit,
  editPost,
  remove,
};
