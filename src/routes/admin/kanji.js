const {getAllKanji, getKanjiCount,
  getKanjiById, updateKanji,
  createKanji} = require('../../services/kanji');

const objectName = 'Kanji';


async function list(req, res) {
  let objects;
  const pageSize = 50;
  let pageNumber;
  const count = await getKanjiCount();

  const columns = [
    {name: 'Kanji', value: 'letter'},
    {name: 'Kunyomi', value: 'kunyomi'},
    {name: 'Onyomi', value: 'onyomi'},
    {name: 'Meaning', value: 'meaning'},
  ];
  const itemActions = [
    {
      text: 'View',
      link: '/kanji/view/<<:id>>',
    },
    {
      text: 'Edit',
      link: '/admin/kanji/<<:id>>',
    },
  ];

  const actions = [];

  if (req.query.page !== undefined && Number(req.query.page > 0)) {
    objects = await getAllKanji(pageSize, Number(req.query.page) - 1);
    pageNumber = Number(req.query.page)- 1;
  } else {
    objects = await getAllKanji(pageSize, 0);
    pageNumber = Number(0);
  }

  res.render('pages/admin-list.njk', {
    user: req.user,
    objects,
    pageSize,
    pageNumber,
    count,
    actions,
    itemActions,
    columns,
    objectName,
  });
};

async function edit(req, res) {
  const objectPostRoute = '/admin/kanji';
  const object = await getKanjiById(Number(req.params.id));

  if (object.mnemonic == null) {
    object.mnemonic = '';
  }

  const properties = [
    {label: 'Letter', field: 'letter', type: 'text'},
    {label: 'Onyomi', field: 'onyomi', type: 'text'},
    {label: 'Kunyomi', field: 'kunyomi', type: 'text'},
    {label: 'Meaning', field: 'meaning', type: 'text'},
    {label: 'Mnemonic', field: 'mnemonic', type: 'text'},
    {label: 'JLPT', field: 'jlpt', type: 'number'},
    {label: 'Level', field: 'level', type: 'number'},
  ];

  res.render('pages/admin-edit.njk', {
    user: req.user,
    object,
    properties,
    objectName,
    objectPostRoute,
  });
}

async function editPost(req, res) {
  try {
    if (req.body.id !== null && req.body.id > -1) {
      console.log('updating kanji');
      await updateKanji(req.body, req.body.id);
    } else {
      console.log('creating kanji');
      await createKanji(req.body);
    }


    res.redirect('/admin/kanji');
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
  }
};

module.exports = {
  list,
  edit,
  editPost,
};
