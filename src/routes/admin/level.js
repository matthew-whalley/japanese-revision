const {getAllLevels, getLevelCount,
  getLevelById, getEmptyLevel,
  updateLevel, createLevel} = require('../../services/levels');

async function list(req, res) {
  let objects;
  const pageSize = 50;
  let pageNumber;
  const count = await getLevelCount();

  const objectName = 'Levels';
  const columns = [
    {name: 'Id', value: 'id'},
    {name: 'Name', value: 'name'},
    {name: 'Number', value: 'number'},
    {name: 'JLPT', value: 'jlpt'},
  ];
  const itemActions = [
    {
      text: 'Edit',
      link: '/admin/level/<<:id>>',
    },
  ];

  const actions = [{
    text: 'Add Level',
    link: '/admin/level/-1',
  }];

  if (req.query.page !== undefined && Number(req.query.page > 0)) {
    objects = await getAllLevels(pageSize, Number(req.query.page) - 1);
    pageNumber = Number(req.query.page)- 1;
  } else {
    objects = await getAllLevels(pageSize, 0);
    pageNumber = Number(0);
  }

  res.render('pages/admin-list.njk', {
    user: req.user,
    objects,
    pageSize,
    pageNumber,
    count,
    actions,
    itemActions,
    columns,
    objectName,
  });
};

async function edit(req, res) {
  const objectName = 'Level';
  const objectPostRoute = '/admin/level';
  let object;

  if (Number(req.params.id) > -1) {
    object= await getLevelById(Number(req.params.id));
  } else {
    object = getEmptyLevel();
  }

  const properties = [
    {label: 'Name', field: 'name', type: 'text'},
    {label: 'Description', field: 'description', type: 'text'},
    {label: 'Number', field: 'number', type: 'number'},
    {label: 'JLPT', field: 'jlpt', type: 'number'},
  ];

  res.render('pages/admin-edit.njk', {
    user: req.user,
    object,
    properties,
    objectName,
    objectPostRoute,
  });
};

async function editPost(req, res) {
  try {
    if (req.body.id !== null && req.body.id > -1) {
      console.log('updating level');
      await updateLevel(req.body, req.body.id);
    } else {
      console.log('creating level');
      await createLevel(req.body);
    }


    res.redirect('/admin/level');
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
  }
};

module.exports = {
  list,
  edit,
  editPost,
};
