module.exports = async (req, res) => {
  let errors = [];

  if (req.errors) {
    errors = req.errors;
  }

  const tiles = [
    {
      name: 'Kanji',
      id: 'kanji',
      actions: [{text: 'Manage', value: '/admin/kanji'}],
    },
    {
      name: 'Vocabulary',
      id: 'vocabulary',
      actions: [
        {text: 'Manage', value: '/admin/word'},
        {text: 'Add', value: '/admin/word/-1'},
      ],
    },
    {
      name: 'Lessons',
      id: 'lessons',
      actions: [
        {text: 'Manage', value: '/admin/lesson'},
        {text: 'Add', value: '/admin/lesson/-1'},
      ],
    },
    {
      name: 'Users',
      id: 'users',
      actions: [{text: 'Manage', value: '/admin/user'}],
    },
    {
      name: 'Feedback',
      id: 'feedback',
      actions: [{text: 'Manage', value: '/admin/feedback'}],
    },
    {
      name: 'Levels',
      id: 'levels',
      actions: [
        {text: 'Manage', value: '/admin/level'},
        {text: 'Add', value: '/admin/level/-1'},
      ],
    },
    {
      name: 'Quizes',
      id: 'quiz',
      actions: [
        {text: 'Manage', value: '/admin/quiz'},
        {text: 'Add', value: '/admin/quiz/-1'},
      ],
    },
    {
      name: 'Questions',
      id: 'quistions',
      actions: [
        {text: 'Manage', value: '/admin/question'},
        {text: 'Add', value: '/admin/question/-1'},
      ],
    },
  ];

  res.render('pages/admin.njk', {
    user: req.user,
    errors,
    tiles,
  });
};
