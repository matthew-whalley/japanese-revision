const Quiz = require('../../services/quiz');

async function list(req, res) {
  let objects;
  const pageSize = 50;
  let pageNumber;
  const count = await Quiz.krud.getCount();

  const objectName = 'Quiz';
  const columns = [
    {name: 'Route', value: 'route'},
    {name: 'Is Draft', value: 'isDraft'},
    {name: 'Random Order', value: 'randomiseOrder'},
  ];
  const itemActions = [
    {
      text: 'Edit',
      link: '/admin/quiz/<<:id>>',
    },
    {
      text: 'Delete',
      link: '/admin/quiz/<<:id>>/delete',
    },
  ];

  const actions = [{
    text: 'Add Quiz',
    link: '/admin/quiz/-1',
  }];

  if (req.query.page !== undefined && Number(req.query.page > 0)) {
    objects = await Quiz.krud.getAll(Number(req.query.page) - 1, pageSize);
    pageNumber = Number(req.query.page)- 1;
  } else {
    objects = await Quiz.krud.getAll(0, pageSize);
    pageNumber = Number(0);
  }


  res.render('pages/admin-list.njk', {
    user: req.user,
    objects,
    pageSize,
    pageNumber,
    count,
    objectName,
    actions,
    columns,
    itemActions,
  });
};

async function edit(req, res) {
  let object;
  const objectName = 'Quiz';
  const objectPostRoute = '/admin/quiz';

  if (Number(req.params.id) > -1) {
    object = await Quiz.krud.getById(Number(req.params.id));
  } else {
    object = Quiz.krud.getEmpty();
  }

  const questions = await Quiz.getQuestions(req.params.id);

  const properties = [
    {label: 'route', field: 'route', type: 'text'},
    {label: 'topics', field: 'topics', type: 'text'},
    {label: 'length', field: 'length', type: 'number'},
    {
      label: 'questions',
      field: 'questions',
      type: 'multi',
      data: questions,
      fields: ['id', 'name'],
    },
  ];


  res.render('pages/admin-edit.njk', {
    user: req.user,
    object,
    properties,
    objectName,
    objectPostRoute,
  });
};

async function editPost(req, res) {
  try {
    if (req.body.id !== null && req.body.id > -1) {
      console.log('updating quiz');
      await Quiz.krud.update(req.body, req.body.id);
    } else {
      console.log('adding quiz');
      await Quiz.krud.create(req.body);
    }

    res.redirect('/admin/quiz');
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
  }
};

async function remove(req, res) {
  console.log(req.params.id);

  await Quiz.krud.remove(req.params.id);

  res.redirect('/admin/quiz');
};

module.exports = {
  list,
  edit,
  editPost,
  remove,
};
