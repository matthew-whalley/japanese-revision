const {getAllUsers, getUserCount, deleteUser} = require('../../services/users');

async function list(req, res) {
  let objects;
  const pageSize = 50;
  let pageNumber;
  const count = await getUserCount();

  const objectName = 'Users';
  const columns = [
    {name: 'Id', value: 'id'},
    {name: 'Username', value: 'username'},
    {name: 'Is admin', value: 'isAdmin'},
  ];
  const itemActions = [
    {
      text: 'Delete',
      link: '/admin/user/<<:id>>/delete',
    },
  ];

  const actions = [];

  if (req.query.page !== undefined && Number(req.query.page > 0)) {
    objects = await getAllUsers(pageSize, Number(req.query.page) - 1);
    pageNumber = Number(req.query.page)- 1;
  } else {
    objects = await getAllUsers(pageSize, 0);
    pageNumber = Number(0);
  }

  res.render('pages/admin-list.njk', {
    user: req.user,
    objects,
    pageSize,
    pageNumber,
    count,
    objectName,
    actions,
    columns,
    itemActions,
  });
};

async function remove(req, res) {
  console.log(req.params.id);

  await deleteUser(req.params.id);

  res.redirect('/admin/user');
};

module.exports = {
  list,
  remove,
};
