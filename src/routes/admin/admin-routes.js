
function generateRoute(baseRoute) {
  return {
    list: baseRoute,
    remove: baseRoute + '/:id/delete',
    edit: baseRoute + '/:id',
    editPost: baseRoute,
  };
}

module.exports = {
  generateRoute,
  AdminRoutes: {
    kanji: generateRoute('/kanji'),
    user: generateRoute('/user'),
    word: generateRoute('/word'),
    feedback: generateRoute('/feedback'),
    level: generateRoute('/level'),
    lesson: generateRoute('/lesson'),
    quiz: generateRoute('/quiz'),
    question: generateRoute('/question'),
  },
};
