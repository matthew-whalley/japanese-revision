const Lessons = require('../../services/lessons');
const {removeExtension} = require('../../utils/misc');

async function list(req, res) {
  let objects;
  const pageSize = 50;
  let pageNumber;
  const count = await Lessons.krud.getCount();

  const objectName = 'Lessons';
  const columns = [
    {name: 'Id', value: 'id'},
    {name: 'Name', value: 'name'},
    {name: 'Template name', value: 'templateName'},
    {name: 'Draft', value: 'isDraft'},
    {name: 'Version', value: 'version'},
    {name: 'Level', value: 'LevelId'},
    {name: 'JLPT', value: 'jlpt'},
  ];
  const itemActions = [
    {
      text: 'Edit',
      link: '/admin/lesson/<<:id>>',
    },
  ];

  const actions = [{
    text: 'Add Lesson',
    link: '/admin/lesson/-1',
  }];

  if (req.query.page !== undefined && Number(req.query.page > 0)) {
    objects = await Lessons.krud.getAll(Number(req.query.page) - 1, pageSize);
    pageNumber = Number(req.query.page)- 1;
  } else {
    objects = await Lessons.krud.getAll(0, pageSize);
    pageNumber = Number(0);
  }

  res.render('pages/admin-list.njk', {
    user: req.user,
    objects,
    pageSize,
    pageNumber,
    count,
    objectName,
    actions,
    columns,
    itemActions,
  });
};

async function edit(req, res) {
  const objectName = 'Lesson';
  const objectPostRoute = '/admin/lesson';
  let object;

  if (Number(req.params.id) > -1) {
    object= await Lessons.krud.getById(Number(req.params.id));
  } else {
    object = Lessons.krud.getEmpty();
  }

  const templates = await Lessons.getTemplateNames();

  const templateNameOptions = [];

  templates.forEach((template) => {
    const templateName = removeExtension(template);
    templateNameOptions.push({
      value: templateName,
      name: templateName,
    });
  });

  const properties = [
    {label: 'Name', field: 'name', type: 'text'},
    {
      label: 'Template name',
      field: 'templateName',
      type: 'select',
      options: templateNameOptions,
    },
    {label: 'Description', field: 'description', type: 'text'},
    {label: 'Version', field: 'version', type: 'number'},
    {label: 'Order', field: 'order', type: 'number'},
    {label: 'Level', field: 'LevelId', type: 'number'},
    {label: 'JLPT', field: 'jlpt', type: 'number'},
  ];

  res.render('pages/admin-edit.njk', {
    user: req.user,
    object,
    properties,
    objectName,
    objectPostRoute,
  });
};

async function editPost(req, res) {
  try {
    if (req.body.id !== null && req.body.id > -1) {
      console.log('updating lesson');
      await Lessons.krud.update(req.body, req.body.id);
    } else {
      console.log('creating lesson');
      await Lessons.krud.create(req.body);
    }


    res.redirect('/admin/lesson');
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
  }
};

module.exports = {
  list,
  edit,
  editPost,
};

