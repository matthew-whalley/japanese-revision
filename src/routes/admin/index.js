const express = require('express');

const admin = require('./admin');
const {
  AdminRoutes,
} = require('./admin-routes');

const defaultGetRoutes = ['list', 'edit', 'remove'];
const defaultPostRoutes = ['editPost'];

function generateAdminGetRoutes(controllerPath, router, key,
    routes = defaultGetRoutes) {
  const ctrl = require(controllerPath);

  routes.forEach((route) =>{
    router.get(AdminRoutes[key][route], ctrl[route]);
  });
}

function generateAdminPostRoutes(controllerPath, router, key,
    routes = defaultPostRoutes) {
  const ctrl = require(controllerPath);

  routes.forEach((route) =>{
    router.post(AdminRoutes[key][route], ctrl[route]);
  });
}

// eslint-disable-next-line new-cap
const router = express.Router();

router.get('/', admin);

generateAdminGetRoutes('./kanji', router, 'kanji', ['list', 'edit']);
generateAdminPostRoutes('./kanji', router, 'kanji');

generateAdminGetRoutes('./user', router, 'user', ['list', 'remove']);

generateAdminGetRoutes('./word', router, 'word');
generateAdminPostRoutes('./word', router, 'word');

generateAdminGetRoutes('./quiz', router, 'quiz');
generateAdminPostRoutes('./quiz', router, 'quiz');

generateAdminGetRoutes('./questions', router, 'question');
generateAdminPostRoutes('./questions', router, 'question');


generateAdminGetRoutes('./feedback', router, 'feedback', ['list', 'edit']);

generateAdminGetRoutes('./level', router, 'level', ['list', 'edit']);
generateAdminPostRoutes('./level', router, 'level');

generateAdminGetRoutes('./lesson', router, 'lesson', ['list', 'edit']);
generateAdminPostRoutes('./lesson', router, 'lesson');

module.exports = router;
