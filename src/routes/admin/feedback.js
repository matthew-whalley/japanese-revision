const {getAllFeedback, getFeedbackCount,
  getFeedbackById} = require('../../services/feedback');

const objectName = 'Feedback';


async function list(req, res) {
  let objects;
  const pageSize = 50;
  let pageNumber;
  const count = await getFeedbackCount();

  const columns = [
    {name: 'id', value: 'id'},
    {name: 'Reporter email', value: 'email'},
    {name: 'Consent', value: 'consent'},
    {name: 'Submitted', value: 'createdAt'},
  ];
  const itemActions = [
    {
      text: 'View',
      link: '/admin/feedback/<<:id>>',
    },
  ];

  const actions = [];

  if (req.query.page !== undefined && Number(req.query.page > 0)) {
    objects = await getAllFeedback(pageSize, Number(req.query.page) - 1);
    pageNumber = Number(req.query.page)- 1;
  } else {
    objects = await getAllFeedback(pageSize, 0);
    pageNumber = Number(0);
  }

  res.render('pages/admin-list.njk', {
    user: req.user,
    objects,
    pageSize,
    pageNumber,
    objectName,
    columns,
    itemActions,
    actions,
    count,
  });
};


async function edit(req, res) {
  console.log(req.params.id);

  const feedback = await getFeedbackById(Number(req.params.id));

  res.render('pages/admin-feedback-view.njk', {
    user: req.user,
    feedback,
  });
};

module.exports = {
  list,
  edit,
};
