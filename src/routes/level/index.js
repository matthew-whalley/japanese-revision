const express = require('express');
const routes = require('../routes');

const level = require('./level');

// eslint-disable-next-line new-cap
const router = express.Router();

router.get(routes.LEVEL, level);

module.exports = router;
