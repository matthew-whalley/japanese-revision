const {
  getLevelByNumber,
} = require('../../services/levels');
const Lessons = require('../../services/lessons');

module.exports = async (req, res) => {
  const levelNumber = req.params.level;

  const level = await getLevelByNumber(levelNumber);
  const lessons = await Lessons.getLessonsByLevel(level.id);

  res.render('pages/level.njk', {
    user: req.user,
    level,
    lessons,
  });
};


