module.exports = (req, res)=>{
  res.render('pages/feedback.njk', {
    user: req.user,
    submitted: false,
  });
};
