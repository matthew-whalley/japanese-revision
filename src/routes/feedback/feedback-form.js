const {addFeedback} = require('../../services/feedback');
const {validateEmail} = require('../../utils/misc');

module.exports = async (req, res) => {
  const email = req.body.email.substring(0, 320);

  if (req.body.email !== '' && !validateEmail(email)) {
    res.render('pages/feedback.njk', {
      user: req.user,
      errors: {
        email: 'Invalid email format',
      },
      submitted: false,
    });
    return;
  }

  const consent = req.body.consent === 'on';

  await addFeedback(email, req.body.feedback, consent);

  res.render('pages/feedback.njk', {
    user: req.user,
    submitted: true,
  });
};
