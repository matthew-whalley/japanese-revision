const express = require('express');
const routes = require('../routes');

const feedbackPageHandler = require('./feedback-page');
const feedbackFormHandler = require('./feedback-form');

// eslint-disable-next-line new-cap
const feedbackRouter = express.Router();

feedbackRouter.get(routes.FEEDBACK, feedbackPageHandler);

feedbackRouter.post(routes.FEEDBACK, [
  express.json(),
  feedbackFormHandler,
]);


module.exports = feedbackRouter;
