const {
  kanji,
  vocabulary,
} = require('../../services');

module.exports = async (req, res) => {
  console.log(req.params.id);
  const character = await kanji.getKanjiById(req.params.id);
  const words = await vocabulary.getVocabularyByKanji(character.letter);
  res.render('pages/kanji-view.njk', {
    user: req.user,
    kanji: character,
    words,
  });
};
