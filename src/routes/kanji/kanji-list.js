const {
  progress,
  levels,
} = require('../../services');
const {reviewLevels,
  getLevelDescriptionFromSchedule} = require('../../utils/misc');

module.exports = async (req, res) => {
  const lvls = await levels.getAllLevels();

  const kanjiProgress = await progress.getKanjiProgress(req.user.id);

  const kanjiProgressSplit = await progress.getKanjiReviewStages(req.user.id);

  const progressSplitLabels = [];
  const progressSplitData = [];

  let levelCount = 0;

  for (const key of Object.keys(reviewLevels)) {
    for (const split of kanjiProgressSplit) {
      const count = split.dataValues.kanji;
      const levelNum = split.dataValues.reviewNumber;
      if (
        getLevelDescriptionFromSchedule(levelNum) === key
      ) {
        levelCount = levelCount + count;
      }
    }

    progressSplitLabels.push(key);
    progressSplitData.push(levelCount);
    levelCount = 0;
  }


  const kanjiReviews = kanjiProgress.length;

  res.render('pages/kanji-list.njk', {
    user: req.user,
    levels: lvls,
    kanjiProgress,
    kanjiReviews,
    progressSplitData,
    progressSplitLabels,
  });
};
