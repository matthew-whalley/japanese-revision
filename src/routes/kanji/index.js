const express = require('express');

const routes = require('../routes');

const authenticate = require('../../middleware/authenticate');


const kanjiList = require('./kanji-list');
const kanjiView = require('./kanji-view');
const addKanji = require('./add-kanji');
const kanjiQuiz = require('./kanji-quiz');


// eslint-disable-next-line new-cap
const router = express.Router();

router.get(routes.KANJI, kanjiList);
router.get(routes.KANJI + '/view/:id', kanjiView);

router.post(routes.KANJI + '/add', [authenticate, addKanji]);
router.use(routes.KANJI + '/quiz', [authenticate, kanjiQuiz]);


module.exports = router;
