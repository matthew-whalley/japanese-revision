const {Quiz} = require('../../utils/quiz');
const {
  progress,
} = require('../../services');
const {isCorrectFromList} = require('../../utils/misc');


async function generateQuestions(userId) {
  const questions = [];
  const kanjiProgress = await progress.getKanjiProgress(userId);

  let qid = 0;

  kanjiProgress.forEach((progress)=>{
    questions.push({
      type: 'kanji',
      id: qid,
      kanji: progress.Kanji.id,
      question: `${progress.Kanji.letter} meaning`,
      answer: progress.Kanji.meaning,
      expectJapanese: false,
      tags: ['meaning'],
      isCorrect: isCorrectFromList,
    });

    qid++;

    questions.push({
      type: 'kanji',
      id: qid,
      kanji: progress.Kanji.id,
      question: `${progress.Kanji.letter} readings`,
      answer: progress.Kanji.onyomi + ',' + progress.Kanji.kunyomi,
      expectJapanese: true,
      tags: ['reading'],
      isCorrect: isCorrectFromList,
    });

    qid++;
  });

  return questions;
}

async function onQuizLoad(req, res, quiz) {
  // access to the req, response and the quiz
  console.log('Kanji Quiz');
  quiz.questions = await generateQuestions(req.user.id);
  if (quiz.questions.length == 0) {
    res.redirect('/dashboard');
  }

  for (const key in req.session) {
    if (key.startsWith('kanji')) delete req.session[key];
  }
}

async function onPageLoad(req, res) {
  console.log('hitting page');
}

async function onCorrectAnswer(question, req, userId) {
  console.log('using new on correct handler');

  if (req.session[`kanji-${question.kanji}`] == undefined) {
    req.session[`kanji-${question.kanji}`] = {};
  }

  if (question.tags.includes('meaning')) {
    req.session[`kanji-${question.kanji}`].meaning = true;
  }

  if (question.tags.includes('reading')) {
    req.session[`kanji-${question.kanji}`].reading = true;
  }

  req.session.save();

  const kanjiStatus = req.session[`kanji-${question.kanji}`];

  if (kanjiStatus.meaning == true && kanjiStatus.reading == true) {
    await progress.kanjiAnsweredCorrect(userId, question.kanji);
  }

  return true;
}

async function onIncorrectAnswer(question, req, userId) {
  console.log('using new on inCorrect handler');

  if (req.session[`kanji-${question.kanji}`] == undefined) {
    req.session[`kanji-${question.kanji}`] = {};
  }

  if (question.tags.includes('meaning')) {
    req.session[`kanji-${question.kanji}`].meaning = false;
  }

  if (question.tags.includes('reading')) {
    req.session[`kanji-${question.kanji}`].reading = false;
  }

  req.session.save();

  const kanjiStatus = req.session[`kanji-${question.kanji}`];

  // if they have answered both reading and meaning and
  // got at least one wrong then mark kanji as incorrect
  if ((kanjiStatus.meaning == false || kanjiStatus.reading == false) &&
    (kanjiStatus.reading !== undefined && kanjiStatus.meaning !== undefined)) {
    await progress.kanjiAnsweredIncorrect(userId, question.kanji);
  }

  return true;
}

const sampleQuiz = new Quiz({
  route: '/',
  config: {
    requireAuth: false,
    title: 'Kanji Quiz',
  },
  onQuizLoad,
  onPageLoad,
  onCorrectAnswer,
  onIncorrectAnswer,
});


module.exports = sampleQuiz.getRouter();
