const {assignKanjiBatch} = require('../../services/users');
const kanjiList = require('./kanji-list');

module.exports = async (req, res) => {
  try {
    console.log('adding kanji');
    console.log('user id is ' + req.user.id);
    const kanjiInserted = await assignKanjiBatch(req.user.id, 5);
    console.log(`assigned ${kanjiInserted} kanji to user`);
    await kanjiList(req, res);
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
  }
};
