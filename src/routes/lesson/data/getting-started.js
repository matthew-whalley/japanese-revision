function data() {
  return {
    words: [],
  };
}


const contents = [
  {
    name: 'Getting started',
    href: '#getting-started',
  },
  {
    name: 'Introduction',
    href: '#introduction',
  },
];

module.exports = {
  data,
  contents,
};
