function data() {
  return {
    words: [],
  };
}


const contents = [
  {
    name: 'Looks like with adjectives',
    href: '#section-1',
  },
  {
    name: 'Doesn\'t Looks like with adjectives',
    href: '#section-2',
  },
  {
    name: 'Looks like with verbs',
    href: '#section-3',
  },
  {
    name: 'Looks like with nouns',
    href: '#section-4',
  },
];

module.exports = {
  data,
  contents,
};
