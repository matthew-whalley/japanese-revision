function data() {
  return {
    words: [],
  };
}


const contents = [
  {
    name: 'Te Form',
    href: '#te-form',
  },
  {
    name: 'Ta form',
    href: '#ta-form',
  },
  {
    name: 'Because',
    href: '#because',
  },
  {
    name: 'Already and not yet',
    href: '#already',
  },
  {
    name: 'Comparing two items',
    href: '#comparison-two',
  },
  {
    name: 'Saying what you plan to do',
    href: '#planning',
  },
  {
    name: 'Any/Some thing/one/where',
    href: '#anything',
  },
  {
    name: 'Describing difficulty',
    href: '#hard-easy',
  },
  {
    name: 'Want To',
    href: '#want-to',
  },
  {
    name: 'Combining Nouns',
    href: '#combining-nouns',
  },
  {
    name: 'Creating Explanations',
    href: '#explaining-things',
  },
];

module.exports = {
  data,
  contents,
};
