function data() {
  return {
    words: [],
  };
}


const contents = [
  {
    name: 'X is Y',
    href: '#what-it-is',
    sublinks: [
      {
        name: 'Examples',
        href: '#example-1',
      },
    ],
  },
  {
    name: 'Is it really?',
    href: '#is-it-really',
  },
];

module.exports = {
  data,
  contents,
};
