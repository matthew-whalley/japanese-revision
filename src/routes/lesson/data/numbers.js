function data() {
  return {
    words: [],
  };
}


const contents = [
  {
    name: 'Numbers',
    href: '#heading',
  },
  {
    name: 'Counting up to 10',
    href: '#ten',
  },
  {
    name: 'Counting up to 100',
    href: '#hundred',
  },
  {
    name: 'Counting up to 1000',
    href: '#thousand',
  },
  {
    name: 'Counting up to a million',
    href: '#million',
  },
  {
    name: 'Review',
    href: '/lesson/numbers/quiz-1',
  },
];

module.exports = {
  data,
  contents,
};
