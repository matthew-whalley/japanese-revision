function data() {
  return {
    words: [],
  };
}


const contents = [
  {
    name: 'Hours',
    href: '#hours',
  },
  {
    name: 'Minutes',
    href: '#minutes',
  },
  {
    name: 'Seconds',
    href: '#seconds',
  },
];

module.exports = {
  data,
  contents,
};
