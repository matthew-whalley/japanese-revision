function data() {
  return {
    words: [],
  };
}


const contents = [
  {
    name: 'A, I, U, E, O',
    href: '#vowels',
  },
  {
    name: 'K row',
    href: '#K-row',
  },
  {
    name: 'S row',
    href: '#S-row',
  },
];

module.exports = {
  data,
  contents,
};
