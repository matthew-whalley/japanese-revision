const express = require('express');
const routes = require('../routes');

const lessonHandler = require('./handler');

// import quizes
const numbers1 = require('./quizes/numbers-1');

// eslint-disable-next-line new-cap
const lessonRouter = express.Router();

lessonRouter.use(
    routes.LESSON + '/numbers/quiz-1',
    numbers1.getRouter());

lessonRouter.get(routes.LESSON + '/:lesson', lessonHandler);

module.exports = lessonRouter;
