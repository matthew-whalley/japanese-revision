const Lessons = require('../../services/lessons');

const Routes = require('../../routes/routes');

module.exports = async (req, res) => {
  const lessonSlug = req.params.lesson;

  const lesson = await Lessons.getLessonByLink(lessonSlug);

  if (!req.user.isAdmin && lesson.isDraft) {
    res.redirect(Routes.DASHBOARD);
    return;
  }

  let data = {};
  let contents = [];

  // pass in lesson data if it exists
  try {
    const lessonData = require(`./data/${lessonSlug}.js`);
    data = lessonData.data();
    contents = lessonData.contents;
  } catch (ex) {
    console.log('no data for page');
  }

  const nextLesson = await Lessons.getNextLesson(lesson.id);
  const previousLesson = await Lessons.getPreviousLesson(lesson.id);

  const pageRenderData = {
    user: req.user,
    data,
    lesson,
    contents,
    templateName: lessonSlug,
  };

  if (nextLesson !== null) {
    pageRenderData.nextLessonSlug = nextLesson.templateName;
  }

  if (previousLesson !== null) {
    pageRenderData.previousLessonSlug = previousLesson.templateName;
  }

  res.render(`layouts/lesson.njk`, pageRenderData);
};
