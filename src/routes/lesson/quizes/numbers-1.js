const {Quiz, QuestionGenerator} = require('../../../utils/quiz');


// add quizes here
const qgenerator = new QuestionGenerator();

qgenerator.addGenericQuestion(
    'How do you say 1 in Japanese (In hiragana)',
    'いち',
    true,
);

qgenerator.addGenericQuestion(
    'How do you say 21 in Japanese (In hiragana)',
    'にじゅういち',
    true,
);

qgenerator.addGenericQuestion(
    'How do you say 30 in Japanese (In hiragana)',
    'さんじゅう',
    true,
);

qgenerator.addGenericQuestion(
    'How do you say 324 in Japanese (In hiragana)',
    'さんびゃくにじゅうよん',
    true,
);

const numbers1 = new Quiz({
  route: '/',
  questions: qgenerator.getQuestions(),
  config: {
    requireAuth: false,
  },
});

module.exports = numbers1;
