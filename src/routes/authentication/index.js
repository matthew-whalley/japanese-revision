const express = require('express');
const {createUser} = require('../../services/users');
const authenticationHandler = require('../../utils/authenticationHandler');
const sessionHandler = require('../../utils/sessionHandler');


// eslint-disable-next-line new-cap
const router = express.Router();
sessionHandler.setup(router);
authenticationHandler.setup(router);

router.get('/login', (req, res)=>{
  res.render('pages/login.njk', {}, (err, html)=>{
    res.clearCookie('connect.sid', {path: '/'}).send(html);
  });
});

router.post('/login', (req, res, next)=>{
  res.clearCookie('connect.sid', {path: '/'});
  next();
},
authenticationHandler.login());

router.post('/register', async function(req, res) {
  await createUser(req.body.username, req.body.password);
});

router.post('/logout',
    (req, res)=>{
      req.logOut(()=>{
        req.session.destroy( (err) => {
          res.clearCookie('connect.sid', {path: '/'})
              .render('pages/login.njk', {
                alerts: [
                  {
                    message: 'Logged Out Successfully',
                    type: 'success',
                  },
                ],
              }, (err, html)=>{
                res.clearCookie('connect.sid', {path: '/'}).send(html);
              });
        });
      });
    });

module.exports = router;
