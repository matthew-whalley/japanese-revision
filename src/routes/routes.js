module.exports = {
  DASHBOARD: '/dashboard',
  LEVEL: '/level/:level',
  LESSON: '/lesson',
  FEEDBACK: '/feedback',
  ACCOUNT: '/account',
  KANJI: '/kanji',
  VOCABULARY: '/level/:level/vocabulary',
  ADMIN: '/admin',
};
