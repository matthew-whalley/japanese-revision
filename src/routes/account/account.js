module.exports = (req, res) =>{
  res.render('pages/account.njk', {
    user: req.user,
  });
};
