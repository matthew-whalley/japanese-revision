const express = require('express');
const routes = require('../routes');

const authenticate = require('../../middleware/authenticate');


const accountPageHandler = require('./account');

// eslint-disable-next-line new-cap
const accountRouter = express.Router();

// make sure user is authenticated before allowing any future requests
// to admin area
accountRouter.use(authenticate);

accountRouter.get(routes.ACCOUNT, accountPageHandler);


module.exports = accountRouter;
