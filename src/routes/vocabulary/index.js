const express = require('express');

const routes = require('../routes');

const vocabularyList = require('./vocabulary-list');

// eslint-disable-next-line new-cap
const router = express.Router();

router.get(routes.VOCABULARY, vocabularyList);

module.exports = router;
