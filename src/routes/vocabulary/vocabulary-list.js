const {getVocabularyByLevel} = require('../../services/vocabulary');
const {
  getLevelByNumber,
} = require('../../services/levels');

module.exports = async (req, res) => {
  const levelNumber = req.params.level;
  const level = await getLevelByNumber(levelNumber);
  const words = await getVocabularyByLevel(level.id);

  res.render('pages/vocabulary-list.njk', {
    user: req.user,
    levelNumber,
    words,
  });
};
