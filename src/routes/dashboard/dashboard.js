const {
  levels,
  progress,
} = require('../../services');

module.exports = async (req, res) => {
  const lvls = await levels.getAllLevels();

  const kanjiProgress = await progress.getKanjiProgress(req.user.id);

  const kanjiReviews = kanjiProgress.length;

  res.render('pages/index.njk', {
    user: req.user,
    levels: lvls,
    kanjiProgress,
    kanjiReviews,
  }, (err, html)=>{
    console.log(err);
    res.send(html);
  });
};
