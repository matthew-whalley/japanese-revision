const express = require('express');
const authenticate = require('../../middleware/authenticate');
const routes = require('../routes');

const dashboard = require('./dashboard');

// eslint-disable-next-line new-cap
const router = express.Router();

router.use(authenticate);

router.get(routes.DASHBOARD, dashboard);

module.exports = router;
