// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

Cypress.Commands.add('login', (username, password) => {
  cy.request({
    method: 'POST',
    url: '/login',
    form: true,
    followRedirect: false,
    body: {
      username: username,
      password: password,
    },
  });
  cy.visit('/dashboard');
});

Cypress.Commands.add('logout', () => {
  cy.get('#logout').click();
});
