describe('Scenario: Managing Vocabulary with the admin panel', () => {
  context(('Logged in as an admin user'), ()=>{
    beforeEach(() => {
      cy.login('admin', 'test');
    });

    it('admin page should contain the tiles with the expected actions', ()=>{
      cy.visit('/admin');
      cy.get('#kanji').contains('Manage').should('be.visible');
      cy.get('#vocabulary').contains('Manage').should('be.visible');
      cy.get('#vocabulary').contains('Add').should('be.visible');
      cy.get('#lessons').contains('Manage').should('be.visible');
      cy.get('#lessons').contains('Add').should('be.visible');
      cy.get('#users').contains('Manage').should('be.visible');
      cy.get('#feedback').contains('Manage').should('be.visible');
      cy.get('#levels').contains('Manage').should('be.visible');
      cy.get('#levels').contains('Add').should('be.visible');
    });

    it('should be able to create and then remove a word', ()=>{
      cy.visit('/admin');
      cy.get('#vocabulary').contains('Add').click();
      cy.get('input[name=meaning]').type('One');
      cy.get('input[name=hiragana]').type('いち');
      cy.get('input[name=kanji]').type('一');
      cy.get('input[name=romaji]').type('ichi');
      cy.get('#edit-form').submit();
      cy.visit('/admin/word');
      cy.get('table tr').should('have.length', 2);
      cy.get('table tr').contains('Delete').click();
      cy.get('table tr').should('have.length', 1);
    });
  });
});
