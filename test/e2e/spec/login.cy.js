describe('Scenario: Authentication', () => {
  it('Should log in correctly if correct credentials are used', ()=>{
    cy.visit('/login');
    cy.get('#login-form input[name=username]').type('test');
    cy.get('#login-form input[name=password]').type('test');
    cy.get('#login-form').submit();
    cy.location('pathname').should('include', 'dashboard');
  });

  context('Logged in as a non-admin user', () =>{
    beforeEach(() => {
      cy.login('admin', 'test');
      cy.visit('/dashboard');
    });

    it('should contain a link to the account page', () => {
      cy.get('.navbar-nav').contains('Account').should('be.visible');
    });

    it('should contain a logout link', () => {
      cy.get('.navbar-nav').contains('Logout').should('be.visible');
    });
  });

  context('Logged in as an admin user', () =>{
    beforeEach(() => {
      cy.login('admin', 'test');
    });

    it('should contain a link to the account page and admin page', () => {
      cy.get('.navbar-nav').contains('Account').should('be.visible');
      cy.get('.navbar-nav').contains('Admin').should('be.visible');
    });
  });

  it('should notify user if they log out', () => {
    cy.login('admin', 'test');
    cy.logout();
    cy.get('.alert.alert-success')
        .should('contain', 'Logged Out Successfully');
  });
});
