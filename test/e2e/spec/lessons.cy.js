describe('Scenario: Viewing the lessons', () => {
  context(('Logged in as a user'), ()=>{
    beforeEach(() => {
      cy.login('test', 'test');
    });

    it('should be able to navigate to the lessons from dashboard', () => {
      cy.get('#levels a').first().click();
      cy.get('#lessons a').first().click();

      // should have table of contents and the lesson title displayed
      cy.get('h2').first().should('contain', 'Test Lesson');
    });

    it('lesson should have table of contents', ()=>{
      cy.visit('/lesson/test');
      cy.get('table tr')
          .should('not.have.length', 0);
    });
  });
});
