const {expect} = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

describe('src/cron-jobs/update-progress', ()=>{
  it('should update kanji that need reviewing', async ()=>{
    const progress = {
      lastReview: new Date('2021-09-21T11:51:00'),
      reviewNumber: 0,
      forReview: false,
      save: ()=>{},
    };

    const updateProgressCronJob = proxyquire(
        '../../../../src/cron-jobs/update-progress', {
          '../services/progress': {
            getAllKanjiProgress: sinon.fake.resolves([
              progress,
            ]),
          },
          '../utils/misc': {
            needsReview: ()=>{
              return true;
            },
          },
        },
    );

    await updateProgressCronJob.handler();


    expect(progress.forReview).to.equal(true);
  });

  afterEach(() => {
    sinon.reset();
    sinon.restore();
  });
});
