const {
  assert,
} = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

describe('/src/services/users', () => {
  describe('assignKanjiBatch', () => {
    it('should create kanji progress if user has no kanji assigned',
        async () => {
          const fakeBulkCreate = sinon.fake();

          const assignKanji = proxyquire('../../../../src/services/users.js', {
            '../models': {
              User: {
                findByPk: sinon.fake.returns({
                  latestKanjiId: 0,
                  update: sinon.fake(),
                }),
              },
              KanjiProgress: {
                bulkCreate: fakeBulkCreate,
              },
              sequelize: {
                transaction: () => {
                  return {
                    commit: sinon.fake(),
                    rollback: sinon.fake(),
                  };
                },
              },
            },
            './kanji': {
              getKanjiById: () => {
                return {};
              },
            },
          }).assignKanjiBatch;

          await assignKanji(1);

          assert.deepEqual(fakeBulkCreate.args[0][0], [{
            UserId: 1,
            KanjiId: 1,
          },
          {
            UserId: 1,
            KanjiId: 2,
          },
          {
            UserId: 1,
            KanjiId: 3,
          },
          {
            UserId: 1,
            KanjiId: 4,
          },
          {
            UserId: 1,
            KanjiId: 5,
          },
          ]);
        },
    );

    it('should create kanji progress if user has kanji already assigned',
        async () => {
          const fakeBulkCreate = sinon.fake();

          const assignKanji = proxyquire('../../../../src/services/users.js', {
            '../models': {
              User: {
                findByPk: sinon.fake.returns({
                  latestKanjiId: 5,
                  update: sinon.fake(),
                }),
              },
              KanjiProgress: {
                bulkCreate: fakeBulkCreate,
              },
              sequelize: {
                transaction: () => {
                  return {
                    commit: sinon.fake(),
                    rollback: sinon.fake(),
                  };
                },
              },
            },
            './kanji': {
              getKanjiById: () => {
                return {};
              },
            },
          }).assignKanjiBatch;

          await assignKanji(1);

          assert.deepEqual(fakeBulkCreate.args[0][0], [{
            UserId: 1,
            KanjiId: 6,
          },
          {
            UserId: 1,
            KanjiId: 7,
          },
          {
            UserId: 1,
            KanjiId: 8,
          },
          {
            UserId: 1,
            KanjiId: 9,
          },
          {
            UserId: 1,
            KanjiId: 10,
          },
          ]);
        },
    );

    it('should update latest kanjiId after assigned kanji',
        async () => {
          const fakeUserUpdate = sinon.fake();

          const assignKanji = proxyquire('../../../../src/services/users.js', {
            '../models': {
              User: {
                findByPk: sinon.fake.returns({
                  latestKanjiId: 0,
                  update: fakeUserUpdate,
                }),
              },
              KanjiProgress: {
                bulkCreate: ()=>{},
              },
              sequelize: {
                transaction: () => {
                  return {
                    commit: sinon.fake(),
                    rollback: sinon.fake(),
                  };
                },
              },
            },
            './kanji': {
              getKanjiById: () => {
                return {};
              },
            },
          }).assignKanjiBatch;

          await assignKanji(1);

          assert.deepEqual(fakeUserUpdate.args[0][0], {latestKanjiId: 5});
        },
    );
  });

  describe('getAllUsers', ()=>{
    it('should call the find all method and calculate the offset', async ()=>{
      const fakeFindAll = sinon.fake.returns('test');
      const getAllUsers = proxyquire('../../../../src/services/users.js', {
        '../models': {
          User: {
            findAll: fakeFindAll,
          },

        },
        './kanji': {
          getKanjiById: sinon.fake.resolves({})
        }
      }).getAllUsers;

      await getAllUsers(20, 1);
      await getAllUsers(20, 0);
      await getAllUsers(10, 3);

      assert.equal(fakeFindAll.args[0][0].offset, 20);
      assert.equal(fakeFindAll.args[0][0].limit, 20);

      assert.equal(fakeFindAll.args[1][0].offset, 0);

      assert.equal(fakeFindAll.args[2][0].offset, 30);
      assert.equal(fakeFindAll.args[2][0].limit, 10);
    });
  });

  describe('getUserCount', ()=>{
    it('should call the count method and return the result', async ()=>{
      const expectedUserCount = 21;
      const fakeCount = sinon.fake.returns(expectedUserCount);
      const getUserCount = proxyquire('../../../../src/services/users.js', {
        '../models': {
          User: {
            count: fakeCount,
          },

        },
        './kanji': {
          getKanjiById: sinon.fake.resolves({})
        }
      }).getUserCount;

      const count = await getUserCount();
      assert.equal(count, expectedUserCount);
    });
  });

  describe('getUserByUsername', ()=>{
    it('should call getUserByUsername and return the result', async ()=>{
      const expectedUserCount = {username: 'test'};
      const fakeFindUser = sinon.fake.returns(expectedUserCount);
      const getUserByUsername = proxyquire(
          '../../../../src/services/users.js', {
            '../models': {
              User: {
                findOne: fakeFindUser,
              },
            },
            './kanji': {
              getKanjiById: sinon.fake.resolves({})
            }
          }).getUserByUsername;

      const user = await getUserByUsername('test');
      assert.equal(user, expectedUserCount);
      assert.deepEqual(fakeFindUser.args[0][0], {
        where: {
          username: 'test',
        },
        attributes: {
          exclude: ['password', 'isAdmin'],
        },
      });
    });
  });

  describe('getUserAuthByUsername', ()=>{
    it('should call getUserAuthByUsername and return the result', async ()=>{
      const expectedUserCount = {username: 'test'};
      const fakeFindUser = sinon.fake.returns(expectedUserCount);
      const getUserAuthByUsername = proxyquire(
          '../../../../src/services/users.js', {
            '../models': {
              User: {
                findOne: fakeFindUser,
              },
            },
            './kanji': {
              getKanjiById: sinon.fake.resolves({})
            }
          }).getUserAuthByUsername;

      const user = await getUserAuthByUsername('test');
      assert.equal(user, expectedUserCount);
      assert.deepEqual(fakeFindUser.args[0][0], {
        where: {
          username: 'test',
        },
        attributes: ['username', 'password', 'isAdmin', 'id'],
      });
    });
  });

  describe('deleteUser', () => {
    it('should update latest kanjiId after assigned kanji',
        async () => {
          const fakeDestory = sinon.fake();
          const deleteUser = proxyquire('../../../../src/services/users.js', {
            '../models': {
              User: {
                destroy: fakeDestory,
              },
              sequelize: {
                transaction: () => {
                  return {
                    commit: sinon.fake(),
                    rollback: sinon.fake(),
                  };
                },
              },
            },
            './kanji': {
              getKanjiById: sinon.fake.resolves({})
            }
          }).deleteUser;

          await deleteUser(1);
          await deleteUser(10);

          assert.deepEqual(fakeDestory.args[0][0], {where: {id: 1}});
          assert.deepEqual(fakeDestory.args[1][0], {where: {id: 10}});
        },
    );

    it('should rollback if it fails',
        async () => {
          const fakeDestory = sinon.fake.rejects();
          const fakeRollback = sinon.fake();
          const deleteUser = proxyquire('../../../../src/services/users.js', {
            '../models': {
              User: {
                destroy: fakeDestory,
              },
              sequelize: {
                transaction: () => {
                  return {
                    commit: sinon.fake(),
                    rollback: fakeRollback,
                  };
                },
              },
            },
            './kanji': {
              getKanjiById: sinon.fake.resolves({})
            }
          }).deleteUser;

          await deleteUser(1);
          assert.equal(fakeRollback.callCount, 1);
        },
    );
  });
});
