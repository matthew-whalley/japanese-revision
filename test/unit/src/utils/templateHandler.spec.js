const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();


describe('src/utils/templateHandler', ()=>{
  const views = [
    './views',
  ];

  const mockNunjucks = {
    FileSystemLoader: function() {
      return 'Loader'; // return the loader
    },
    Environment: function() {
      return {
        express: ()=>{},
        addGlobal: () =>{},
        addFilter: () =>{},
      };
    },
  };

  afterEach(() => {
    sinon.reset();
    sinon.restore();
  });

  it('should set the views property', ()=>{
    const templateHandler = proxyquire(
        '../../../../src/utils/templateHandler.js', {
          'nunjucks': mockNunjucks,
        });

    const app = {
      set: sinon.fake(),
      get: sinon.fake.returns(views),
    };

    templateHandler(app);

    sinon.assert.calledWith(app.set, 'views');
  });

  it('should set the view engine', ()=>{
    const templateHandler = proxyquire(
        '../../../../src/utils/templateHandler.js', {
          'nunjucks': mockNunjucks,
        });

    const app = {
      set: sinon.fake(),
      get: sinon.fake.returns(views),
    };

    templateHandler(app);

    sinon.assert.calledWithExactly(app.set, 'view engine', 'njk');
  });
});
