const {
  assert,
} = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();


function quizGetFunction(quizConfig = {}) {
  const fakeGet = sinon.fake();
  const fakePost = sinon.fake();

  const Quiz = proxyquire('../../../../../src/utils/quiz/quiz.js', {
    'express': {
      json: ()=>{},
      Router: function() {
        return {
          get: fakeGet,
          post: fakePost,
          use: ()=>{},
        };
      },
    },
    '../../middleware/authenticate': ()=>{},
  });

  new Quiz(quizConfig);

  return fakeGet.args[0][1];
}

function quizPostFunction(quizConfig = {}) {
  const fakeGet = sinon.fake();
  const fakePost = sinon.fake();

  const Quiz = proxyquire('../../../../../src/utils/quiz/quiz.js', {
    'express': {
      json: ()=>{},
      Router: function() {
        return {
          get: fakeGet,
          post: fakePost,
          use: ()=>{},
        };
      },
    },
    '../../middleware/authenticate': ()=>{},
  });

  new Quiz(quizConfig);

  return fakePost.args[0][1][1];
}

describe('src/utils/quiz/quiz', ()=>{
  describe('When a quiz is created', ()=>{
    it('should setup the get and post routes', ()=>{
      const fakeGet = sinon.fake();
      const fakePost = sinon.fake();

      const Quiz = proxyquire('../../../../../src/utils/quiz/quiz.js', {
        'express': {
          json: ()=>{},
          Router: function() {
            return {
              get: fakeGet,
              post: fakePost,
              use: ()=>{},
            };
          },
        },
        '../../middleware/authenticate': ()=>{},
      });

      new Quiz({});

      assert.equal(fakeGet.callCount, 1);
      assert.equal(fakeGet.args[0][0], '/');
      assert.equal(fakePost.callCount, 1);
      assert.equal(fakePost.args[0][0], '/');
    });
  });

  describe('When Get request is hit', ()=>{
    it('should call the quizLoad method', async ()=>{
      const onQuizStart = sinon.fake();

      const getHandler = quizGetFunction({
        onQuizLoad: onQuizStart,
        questions: [{id: 0}],
      });

      const req = {};
      const res = {
        render: sinon.fake(),
      };

      await getHandler(req, res);

      assert.equal(onQuizStart.callCount, 1,
          'onQuizLoad should be called when the get request is hit');
    });
  });


  describe('When Post request is hit', ()=>{
    it('should use the isCorrect function', ()=>{
      const fakeIsCorrect = sinon.fake.returns(true);

      const postHandler = quizPostFunction({
        questions: [{id: 0, isCorrect: fakeIsCorrect,
        }],
      });

      const req = {
        body: {
          id: 0,
        },
      };
      const res = {
        render: sinon.fake(),
      };

      postHandler(req, res);

      assert.equal(fakeIsCorrect.callCount, 1, 'isCorrect should be called');
    });

    it('should use the onCorrect function if answer is correct', ()=>{
      const fakeIsCorrect = sinon.fake.returns(true);
      const fakeOnCorrect = sinon.fake();
      const postHandler = quizPostFunction({
        onCorrectAnswer: fakeOnCorrect,
        questions: [
          {
            id: 0,
            isCorrect: fakeIsCorrect,
          },
          {
            id: 1,
          }],
      });

      const req = {
        body: {
          id: 0,
        },
        user: {
          id: 0,
        },
      };
      const res = {
        render: sinon.fake(),
      };

      postHandler(req, res);

      assert.equal(fakeOnCorrect.callCount, 1,
          'onCorrectAnswer should be called');
    });

    it('should use onComplete handler if there are no more questions',
        async ()=>{
          const fakeIsCorrect = sinon.fake.returns(true);
          const fakeOnComplete = sinon.fake();
          const postHandler = quizPostFunction({
            onComplete: fakeOnComplete,
            onCorrectAnswer: ()=> true,
            questions: [
              {
                id: 0,
                isCorrect: fakeIsCorrect,
              }],
          });

          const req = {
            body: {
              id: 0,
            },
            user: {
              id: 0,
            },
          };
          const res = {
            render: sinon.fake(),
          };

          await postHandler(req, res);

          assert.equal(fakeOnComplete.callCount, 1,
              'onComplete should be called');
        });
  });
});
