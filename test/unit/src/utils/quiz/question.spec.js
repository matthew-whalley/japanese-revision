const {assert} = require('chai');
const {QuestionGenerator} = require('../../../../../src/utils/quiz/question');
const {isCorrectFromList} = require('../../../../../src/utils/misc');


describe('src/utils/quiz/question', () =>{
  describe('QuestionGenerator', ()=>{
    it('should create an empty questions list when initialised', ()=>{
      const gen = new QuestionGenerator();
      assert.deepEqual(gen.getQuestions(), []);
    });

    it('should add general questions depending on the config', ()=>{
      const gen = new QuestionGenerator();
      gen.addGenericQuestion('hello', 'world', true);

      assert.deepEqual(gen.getQuestions(), [{
        type: 'general',
        id: 0,
        question: 'hello',
        answer: 'world',
        expectJapanese: true,
        isCorrect: isCorrectFromList,
      }]);
    });

    it('should add kanji questions depending on the config', ()=>{
      const gen = new QuestionGenerator();
      gen.addKanjiQuestion({
        id: 0,
        letter: 'test',
        meaning: 'meaning',
      });

      assert.deepEqual(gen.getQuestions(), [{
        type: 'kanji',
        id: 0,
        kanji: 0,
        question: 'test meaning',
        answer: 'meaning',
        expectJapanese: false,
        isCorrect: isCorrectFromList,
      }]);
    });

    it('should support adding multiple questions', ()=>{
      const gen = new QuestionGenerator();
      gen.addGenericQuestion('hello', 'world', true);
      gen.addGenericQuestion('hello 1', 'world 1', false);

      assert.deepEqual(gen.getQuestions(), [{
        type: 'general',
        id: 0,
        question: 'hello',
        answer: 'world',
        expectJapanese: true,
        isCorrect: isCorrectFromList,
      },
      {
        type: 'general',
        id: 1,
        question: 'hello 1',
        answer: 'world 1',
        expectJapanese: false,
        isCorrect: isCorrectFromList,
      }]);
    });
  });
});
