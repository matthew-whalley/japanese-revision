const {
  assert,
  expect,
} = require('chai');
const sinon = require('sinon');

const utils = require('../../../../src/utils/misc');

describe('src/utils/misc', () => {
  describe('getLevelDescriptionFromSchedule', ()=>{
    it('should return the correct description for review level of lowest level',
        ()=>{
          assert.equal(utils.getLevelDescriptionFromSchedule(1), 'new');
          assert.equal(utils.getLevelDescriptionFromSchedule(2), 'new');
        });

    it('should return the correct description for review level of middle level',
        ()=>{
          assert.equal(utils.getLevelDescriptionFromSchedule(3), 'recognized');
          assert.equal(utils.getLevelDescriptionFromSchedule(4), 'recognized');
        });

    it('should return top status if level is high',
        ()=>{
          assert.equal(utils.getLevelDescriptionFromSchedule(6), 'learnt');
          assert.equal(utils.getLevelDescriptionFromSchedule(7), 'mastered');
        });
  });

  describe('needsReview', () => {
    let clock;

    before(() => {
      clock = sinon.useFakeTimers(new Date('2021-09-21T16:51:00'));
    });

    after(() => {
      clock.restore();
    });

    it('should return true if date difference is greater than 4h at level 0',
        () => {
          assert.equal(utils.needsReview(new Date('2021-09-21T11:51:00'), 0),
              true);
          assert.equal(utils.needsReview(new Date('2021-09-21T12:52:00'), 1),
              false);
          assert.equal(utils.needsReview(new Date('2021-09-21T12:50:00'), 0),
              true);
        });

    it('should return true if date difference is greater than 1d at level 1',
        () => {
          assert.equal(utils.needsReview(new Date('2021-09-22T16:51:00'), 1),
              true);
          assert.equal(utils.needsReview(new Date('2021-09-21T09:51:00'), 2),
              false);
        });

    it('should return false if the review number is too high', () => {
      assert.equal(utils.needsReview(new Date('2021-09-21T07:51:00'), 9),
          false);
      assert.equal(utils.needsReview(new Date('2021-09-21T09:51:00'), 9),
          false);
    });
  });

  describe('compareDates', () => {
    it('should return the number of ms between dates that are a minute apart',
        () => {
          const date1 = new Date('2021-09-21T16:51:00');
          const date2 = new Date('2021-09-21T16:52:00');
          const difference = utils.compareDates(date1, date2);

          assert.equal(difference, 60000);
        });

    it('the difference should always be positive', () => {
      const date1 = new Date('2021-09-21T16:51:00');
      const date2 = new Date('2021-09-21T16:52:00');
      const difference = utils.compareDates(date1, date2);

      const date3 = new Date('2021-09-21T16:53:00');
      const date4 = new Date('2021-09-21T16:51:00');
      const difference2 = utils.compareDates(date3, date4);

      assert.isAtLeast(difference, 0);
      assert.isAtLeast(difference2, 0);
    });
  });

  describe('convertTimeToMS', () => {
    it('should convert seconds to ms', () => {
      assert.equal(utils.convertTimeToMS('1s'), 1000);
    });

    it('should convert multiple seconds to ms', () => {
      assert.equal(utils.convertTimeToMS('20s'), 20000);
    });

    it('should support other time units', () => {
      assert.equal(utils.convertTimeToMS('1min'), 1 * 1000 * 60);
      assert.equal(utils.convertTimeToMS('2w'), 2 * 1000 * 60 * 60 * 7);
    });
  });


  describe('isCorrectFromList', () => {
    it('should return true if the answer matches the correct answer', () => {
      const isCorrect = utils.isCorrectFromList('correct', 'correct');
      assert.isTrue(isCorrect);
    });

    it('should return false if the answer doesn\'t match the correct answer',
        () => {
          const isCorrect = utils.isCorrectFromList('wrong', 'correct');
          assert.isFalse(isCorrect);
        });

    it(`should return true if answer matches a single
     correct answer in a comman seperated list`,
    () => {
      assert.isTrue(
          utils.isCorrectFromList('1', 'One, 1'),
      );
      assert.isTrue(
          utils.isCorrectFromList('1', '1, One'),
      );
    });

    it('should not be case sensitive', () => {
      assert.isTrue(
          utils.isCorrectFromList('one', '1, One'),
      );
    });

    it('should return false if none of the answers are in the correct list',
        () => {
          assert.isFalse(
              utils.isCorrectFromList('1', 'Two, 2'),
          );
        });
  });

  describe('matchContainerInnerText', () => {
    it('Should match the text inside a container', () => {
      expect(utils.matchContainerInnerText('test <<:id>> asd'))
          .to.deep.equal(['id']);
    });

    it('Should match texts inside multiple containers', () => {
      expect(utils.matchContainerInnerText('test <<:id>> asd <<:test>>'))
          .to.deep.equal(['id', 'test']);
    });
  });

  describe('removeExtension', () => {
    it('should remove file extensions from a whole file name', () => {
      assert.equal(utils.removeExtension('example-file.txt'), 'example-file');
      assert.equal(utils.removeExtension('example-file.njk'), 'example-file');
    });

    it('should not change the filename if it does not have a file type', () => {
      assert.equal(utils.removeExtension('example-file'), 'example-file');
    });

    it('should only remove the last part', () => {
      assert.equal(
          utils.removeExtension('example-file.spec.js'),
          'example-file.spec',
      );
    });
  });
});
