const {
  expect,
} = require('chai');
const {
  replaceProperties,
} = require('../../../../src/utils/filters');

describe('src/utils/filters', () => {
  describe('replaceProperties', () => {
    it('Should replace a <<:objProperty>> with the object property', () => {
      const replacedString = replaceProperties('hello/<<:property>>', {
        property: 2,
      });
      expect(replacedString).to.equal('hello/2');
    });

    it('Should handle multiple property replacements', () => {
      const replacedString = replaceProperties('hello/<<:prop1>>/<<:prop2>>', {
        prop1: 1,
        prop2: 2,
      });
      expect(replacedString).to.equal('hello/1/2');
    });

    it('Should handle if there are no properties to replace', () => {
      const replacedString = replaceProperties('hello/test', {
        prop1: 1,
        prop2: 2,
      });
      expect(replacedString).to.equal('hello/test');
    });
  });
});
