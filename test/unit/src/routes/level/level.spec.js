const {assert} = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

describe('src/routes/level/level', ()=>{
  it('should render the level template', async ()=>{
    const testLevel = {
      name: 'test',
      number: 0,
      jlpt: 0,
    };

    const testLessons = [
      {
        name: 'Getting started',
        link: 'getting-started',
        levelId: 1,
      },
      {
        name: 'Basic sentences',
        link: 'basic-sentences',
        levelId: 1,
      },
      {
        name: 'Numbers',
        link: 'numbers',
        levelId: 1,
      },
    ];

    const handler = proxyquire(
        '../../../../../src/routes/level/level.js', {
          '../../services/levels': {
            getLevelByNumber: ()=>{
              return new Promise((resolve)=> resolve(testLevel));
            },
          },
          '../../services/lessons': {
            getLessonsByLevel: ()=>{
              return new Promise((resolve)=> resolve(testLessons));
            },
          },
        });

    const req = {
      user: {},
      params: {
        level: 1,
      },
    };

    const fakeRender = sinon.fake();
    const res = {
      render: fakeRender,
    };

    await handler(req, res);

    assert.equal(fakeRender.args[0][0], 'pages/level.njk');
    assert.deepEqual(fakeRender.args[0][1].level, testLevel);
    assert.deepEqual(fakeRender.args[0][1].lessons, testLessons);
  });
});
