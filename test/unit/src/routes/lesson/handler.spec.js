const {assert} = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

describe('src/routes/lesson/handler', ()=>{
  it('should render the template of the lesson', async ()=>{
    const handler = proxyquire(
        '../../../../../src/routes/lesson/handler.js', {
          './data/test.js': {
            data: ()=>{
              return {};
            },
          },
          '../../services/lessons': {
            getLessonByLink: ()=>{
              return new Promise((resolve) => {
                resolve({templateName: 'numbers'});
              });
            },
            getNextLesson: ()=>{
              return new Promise((resolve) =>{
                resolve(null);
              });
            },
            getPreviousLesson: ()=>{
              return new Promise((resolve) =>{
                resolve(null);
              });
            },
          },
        });

    const req = {
      params: {
        lesson: 'numbers',
      },
      user: {
        isAdmin: false,
      },
    };

    const fakeRender = sinon.fake();
    const res = {
      render: fakeRender,
    };

    await handler(req, res);

    assert.equal(fakeRender.args[0][0], 'layouts/lesson.njk');
  });

  it('should fetch lesson data and pass it into the template', async ()=>{
    const sampleData = {
      test: 'test',
    };

    const handler = proxyquire(
        '../../../../../src/routes/lesson/handler.js', {
          './data/test.js': {
            data: ()=>{
              return sampleData;
            },
            contents: [],
          },
          '../../services/lessons': {
            getLessonByLink: ()=>{
              return new Promise((resolve) => {
                resolve({templateName: 'numbers'});
              });
            },
            getNextLesson: ()=>{
              return new Promise((resolve) =>{
                resolve(null);
              });
            },
            getPreviousLesson: ()=>{
              return new Promise((resolve) =>{
                resolve(null);
              });
            },
          },
        });

    const req = {
      user: {
        isAdmin: false,
      },
      params: {
        lesson: 'test',
      },
    };

    const fakeRender = sinon.fake();
    const res = {
      render: fakeRender,
    };

    await handler(req, res);

    assert.deepEqual(fakeRender.args[0][1], {
      user: {
        isAdmin: false,
      },
      data: sampleData,
      contents: [],
      templateName: 'test',
      lesson: {
        templateName: 'numbers',
      },
    });
  });

  it('should not pass in data if file does not exist', async ()=>{
    // setting to null simulates test not existing
    const handler = proxyquire(
        '../../../../../src/routes/lesson/handler.js', {
          './data/test.js': null,
          '../../services/lessons': {
            getLessonByLink: ()=>{
              return new Promise((resolve) => {
                resolve({templateName: 'numbers'});
              });
            },
            getNextLesson: ()=>{
              return new Promise((resolve) =>{
                resolve(null);
              });
            },
            getPreviousLesson: ()=>{
              return new Promise((resolve) =>{
                resolve(null);
              });
            },
          },
        });

    const req = {
      user: {
        isAdmin: false,
      },
      params: {
        lesson: 'test',
      },
    };

    const fakeRender = sinon.fake();
    const res = {
      render: fakeRender,
    };

    await handler(req, res);

    assert.deepEqual(fakeRender.args[0][1], {
      user: {
        isAdmin: false,
      },
      data: {},
      contents: [],
      templateName: 'test',
      lesson: {
        templateName: 'numbers',
      },
    });
  });
});
