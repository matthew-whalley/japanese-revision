const {assert} = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

describe('src/routes/feedback/feedback-form', ()=>{
  it('should render success page if valid form is submitted', async ()=>{
    const handler = proxyquire(
        '../../../../../src/routes/feedback/feedback-form.js', {
          '../../services/feedback': {
            addFeedback: sinon.fake.resolves(),
          },
        });

    const fakeRender = sinon.fake();
    const req = {
      user: null,
      body: {
        email: '',
        feedback: '',
        consent: '',
      },
    };

    const res = {
      render: fakeRender,
    };

    await handler(req, res);

    assert.deepEqual(fakeRender.args[0][1], {
      user: null,
      submitted: true,
    });
  });
});
