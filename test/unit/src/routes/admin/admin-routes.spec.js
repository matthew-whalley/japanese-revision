const {
  assert,
} = require('chai');

const {
  generateRoute,
} = require('../../../../../src/routes/admin/admin-routes');

describe('src/routes/admin/admin-routes', () => {
  it('should generate the krud routes', () => {
    assert.deepEqual(generateRoute('/test'), {
      list: '/test',
      remove: '/test/:id/delete',
      edit: '/test/:id',
      editPost: '/test',
    });
  });
});
