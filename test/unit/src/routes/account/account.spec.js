const {assert} = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

describe('src/routes/account/account', ()=>{
  it('should render the account template', ()=>{
    const handler = proxyquire(
        '../../../../../src/routes/account/account.js', {});

    const req = {
      user: {},
    };

    const fakeRender = sinon.fake();
    const res = {
      render: fakeRender,
    };

    handler(req, res);

    assert.equal(fakeRender.args[0][0], 'pages/account.njk');
  });
  it('should pass the user into the page', ()=>{
    const handler = proxyquire(
        '../../../../../src/routes/account/account.js', {});

    const user = {
      username: 'TEST',
    };

    const req = {
      user,
    };

    const fakeRender = sinon.fake();
    const res = {
      render: fakeRender,
    };

    handler(req, res);

    assert.deepEqual(fakeRender.args[0][1], {user: {
      username: 'TEST',
    }});
  });
});
