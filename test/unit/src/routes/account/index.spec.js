const {assert} = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

describe('src/routes/account/index', ()=>{
  it('should call the authenticate middleware', ()=>{
    const fakeMiddleware = ()=>{};
    const fakeUse = sinon.fake();
    proxyquire(
        '../../../../../src/routes/account/index.js', {
          'express': {
            Router: ()=>{
              return {
                use: fakeUse,
                get: ()=>{},
              };
            },
          },
          '../../middleware/authenticate': fakeMiddleware,
        });

    assert.equal(fakeUse.args[0][0], fakeMiddleware);
  });
  it('should set the account page route', ()=>{
    const fakeMiddleware = ()=>{};
    const fakeAccountHandler = ()=>{};
    const fakeGet = sinon.fake();
    proxyquire(
        '../../../../../src/routes/account/index.js', {
          'express': {
            Router: ()=>{
              return {
                use: ()=>{},
                get: fakeGet,
              };
            },
          },
          './account': fakeAccountHandler,
          '../routes': {ACCOUNT: '/test'},
          '../../middleware/authenticate': fakeMiddleware,
        });
    assert.equal(fakeGet.args[0][0], '/test');
    assert.equal(fakeGet.args[0][1], fakeAccountHandler);
  });
});
