const {
  expect,
} = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

describe('src/middleware/admin-authenticate', () => {
  const res = {
    redirect: sinon.fake(),
    send: sinon.fake(),
  };
  res.status = sinon.fake.returns({
    send: res.send,
  });

  const next = sinon.fake();

  afterEach(() => {
    sinon.reset();
    sinon.restore();
  });

  it('should redirect to login if user isn\'t defined', () => {
    const authenticateMiddleware = proxyquire(
        '../../../../src/middleware/admin-authenticate', {},
    );

    const req = {
      isAuthenticated: ()=>{
        return false;
      },
      log: {
        debug: () =>{},
      }
    };
    authenticateMiddleware(req, res, next);
    expect(res.redirect.calledOnceWith('/login'));
  });

  it('should pass to the page handler if user is defined and is an admin',
      () => {
        const authenticateMiddleware = proxyquire(
            '../../../../src/middleware/admin-authenticate', {},
        );

        const req = {
          isAuthenticated: ()=>{
            return true;
          },
          user: {
            isAdmin: true,
          },
          log: {
            debug: () =>{},
          }
        };
        authenticateMiddleware(req, res, next);
        expect(next.calledOnce);
      });

  it('should return 401 if user is not admin', () => {
    const authenticateMiddleware = proxyquire(
        '../../../../src/middleware/admin-authenticate', {},
    );

    const req = {
      isAuthenticated: ()=>{
        return true;
      },
      log: {
        debug: () =>{},
      },
      user: {
        isAdmin: false,
      },
    };
    authenticateMiddleware(req, res, next);
    expect(res.redirect.calledOnce);
  });
});
