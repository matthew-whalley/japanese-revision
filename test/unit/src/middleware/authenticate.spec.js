const {expect} = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

describe('src/middleware/authenticate', ()=>{
  const res = {
    redirect: sinon.fake(),
  };
  const next = sinon.fake();

  afterEach(() => {
    sinon.reset();
    sinon.restore();
  });

  it('should redirect to login if user isn\'t authenticated', ()=>{
    const authenticateMiddleware = proxyquire(
        '../../../../src/middleware/authenticate',
        {},
    );

    const req = {
      isAuthenticated: ()=> {
        return false;
      },
      log: {
        debug: () =>{},
      }
    };
    authenticateMiddleware(req, res, next);
    expect(res.redirect.calledOnceWith('/login'));
  });

  it('should pass to the page handler if user is authenticated', ()=>{
    const authenticateMiddleware = proxyquire(
        '../../../../src/middleware/authenticate',
        {},
    );

    const req = {
      user: {},
      isAuthenticated: ()=> {
        return true;
      },
    };
    authenticateMiddleware(req, res, next);
    expect(next.calledOnce);
  });
});
