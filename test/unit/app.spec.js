const {
  expect,
} = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

describe('app', () => {
  const fakeUse = sinon.fake();

  const mockAxpressApp = {
    use: fakeUse,
    set: () => {},
    disable: () => {}
  };

  const mockExpress = () => {
    // eslint-disable-next-line no-invalid-this
    this.constructor.prototype.static = function() {};
    return mockAxpressApp;
  };

  afterEach(() => {
    sinon.reset();
    sinon.restore();
  });

  it('should export an express app', () => {
    const app = proxyquire('../../app.js', {
      'express': mockExpress,
      './src/utils/templateHandler': () => {
        return null;
      },
      './src/utils/cronHandler.js': {
        setup: ()=>{},
      },
      './src/routes': () => {
        return null;
      },
    });

    expect(app).to.equal(mockAxpressApp);
  });

  it('should setup the base route', () => {
    proxyquire('../../app.js', {
      'express': mockExpress,
      './src/utils/templateHandler': () => {
        return null;
      },
      './src/utils/cronHandler.js': {
        setup: ()=>{},
      },
      './src/routes': () => {
        return null;
      },
    });

    expect(fakeUse.called).to.be.true;
  });
});

