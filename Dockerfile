ARG PORT

FROM node:18-alpine
ENV PORT=${PORT}
WORKDIR /app
COPY ./package.json /package.json

RUN mkdir /sessions
RUN mkdir /dist
RUN npm install --omit=dev

COPY . /.

EXPOSE ${PORT}
CMD ["npm", "run", "start"]